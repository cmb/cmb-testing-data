ExportSimCTB

To recreate test:
File-Open simplebox.cmb from testing-data
File-Open CTB.sbt from testing-data
Switch to Attribute tab
Click New. Edit row 4. x=6 f(x)=2
Select "Materials" Tab
Click New. Click below the scrollbar 3 times
Associated Piece 0 with the material
Select "Veg" tab
Check "Allow radiation to pass through"
Select "Boundary Conditions" tab
Change attribute to "Specified Flux"
Click New. Select "PolyLinearFunction-0" in the dropdown field
Associated Face4, then Face5
File-Export Simulation File
Select "Veg Model"
Change Level to Advanced
Select "CTB.py" from testing-data as "Python Script"
Save to test.bc in the testing-data folder.
 * Note: Manually edit the script afterward to point to testing/Temporary
Export and Stop Recording
