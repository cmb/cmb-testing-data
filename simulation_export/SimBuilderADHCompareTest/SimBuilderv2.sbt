<SimBuilder Version="2.0">

  <TopLevelGroup><!-- what type of qt object to use is specified elsewhere -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <!-- #################### Functions #################### -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <TopLevel Section="Functions" UIName="Functions" Icon="Function.png">
      <Templates>
        <Instance TagName="Function1DLinear">
        </Instance>
      </Templates>
    </TopLevel>
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <!-- #################### Materials #################### -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <TopLevel Section="Materials" UIName="Materials">
      <Templates>
	<Instance TagName="SolidMaterial" UIName="Material Template">
	  <InformationValue TagName="Porosity" UIName="Porosity:">
            <Group Name="General"/>
            <ValueType Name="double"/>
            <DefaultValue Value="0.5"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
            <Constraint Type="Maximum" Inclusive="Yes" Value="1"/>
	  </InformationValue>
      
          <InformationValue TagName="HydraulicConductivity" UIName="Hydraulic Conductivity:">
            <Group Name="Groundwater Flow"/>
            <Advanced/>
            <MultiValue>
              <InputValue Index="1">
		<ValueType Name="double"/>
		<DefaultValue Value="1"/>
		<Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="2">
		<ValueType Name="double"/>
		<DefaultValue Value="1"/>
		<Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="3">
		<ValueType Name="double"/>
		<DefaultValue Value="1"/>
		<Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="4">
		<ValueType Name="double"/>
		<DefaultValue Value="0"/>
		<Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              <InputValue Index="5">
		<ValueType Name="double"/>
		<DefaultValue Value="0"/>
		<Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              <InputValue Index="6">
		<ValueType Name="double"/>
		<DefaultValue Value="0"/>
		<Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
            </MultiValue>
	  </InformationValue>
      
	  <InformationValue TagName="PresSatCurveIndex" UIName="Pressure-Saturation Curve Index:">
            <Group Name="Groundwater Flow"/>
            <Advanced/>
            <ValueType Name="FunctionId"/>
            <DefaultValue Value="52"/>
	  </InformationValue>
      
	  <InformationValue TagName="PresRelCondCurveIndex" UIName="Pressure-Relative Conductivity Curve Index:">
            <Group Name="Groundwater Flow"/>
            <Advanced/>
            <ValueType Name="FunctionId"/>
            <DefaultValue Value="54"/>
	  </InformationValue>
      
	  <InformationValue TagName="SpecificStorage" UIName="Specific Storage:">
            <Group Name="Groundwater Flow"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="1.0e-6"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
	  </InformationValue>
      
	  <InformationValue TagName="SolidSpecificHeat" UIName="Solids' Specific Heat:" Units="W hr/g-C">
            <Group Name="Heat Transport"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="1.0e-4"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
	  </InformationValue>
      
	  <InformationValue TagName="SolidSpecificGravity" UIName="Solids' Specific Gravity:">
            <Group Name="Heat Transport"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="2.65"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
	  </InformationValue>
      
	  <InformationValue TagName="Albedo" UIName="Albedo (specifies reflected sw radiation):">
            <Group Name="Heat Transport"/>
            <ValueType Name="double"/>
            <DefaultValue Value="0.3"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
            <Constraint Type="Maximum" Inclusive="Yes" Value="1"/>
	  </InformationValue>
      
	  <InformationValue TagName="BulkEmissivity" UIName="Bulk Emissivity:">
            <Group Name="Heat Transport"/>
            <ValueType Name="double"/>
            <DefaultValue Value="0.8"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
            <Constraint Type="Maximum" Inclusive="Yes" Value="1"/>
	  </InformationValue>
      
	  <InformationValue TagName="SolidThermalCondTensor" UIName="Solids' Thermal Conductivity Tensor:" Units="W/m-K">
            <Group Name="Heat Transport"/>
            <MultiValue>
              <InputValue Index="1">
		<ValueType Name="double"/>
		<DefaultValue Value="1"/>
		<Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="2">
		<ValueType Name="double"/>
		<DefaultValue Value="1"/>
		<Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="3">
		<ValueType Name="double"/>
		<DefaultValue Value="1"/>
		<Constraint Type="Minimum" Inclusive="No" Value="0"/>
              </InputValue>
              <InputValue Index="4">
		<ValueType Name="double"/>
		<DefaultValue Value="0"/>
		<Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              <InputValue Index="5">
		<ValueType Name="double"/>
		<DefaultValue Value="0"/>
		<Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              <InputValue Index="6">
		<ValueType Name="double"/>
		<DefaultValue Value="0"/>
		<Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
            </MultiValue>
	  </InformationValue>
      
	  <InformationValue TagName="Tortuosity" UIName="Tortuosity:">
            <Group Name="Heat Transport"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="0.7"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
            <Constraint Type="Maximum" Inclusive="Yes" Value="1"/>
	  </InformationValue>
      
	  <InformationValue TagName="LongitudinalDispersivity" UIName="Longitudinal Dispersivity:">
            <Group Name="Heat Transport"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="1"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
	  </InformationValue>
      
	  <InformationValue TagName="TransverseDispersivity" UIName="Transverse Dispersivity:">
            <Group Name="Heat Transport"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="0.1"/>
            <Constraint Type="Minimum" Inclusive="No" Value="0"/>
	  </InformationValue>
      
	  <InformationValue TagName="MaxRefineLevels" UIName="Maximum Levels of Refinement:">
            <Group Name="General"/>
            <Advanced/>
            <ValueType Name="int"/>
            <DefaultValue Value="0"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
	  </InformationValue>
      
	  <InformationValue TagName="FlowRefineTol" UIName="Refinement Tolerance for Flow:">
            <Group Name="Groundwater Flow"/>
            <Advanced/>
            <ValueType Name="double"/>
            <DefaultValue Value="1"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
	  </InformationValue>
      
          <InformationValue TagName="MaterialRGB" UIName="RGB:">
            <Group Name="Spectral"/>
            <MultiValue>
              <InputValue Index="1">
                <ValueType Name="double"/>
                <DefaultValue Value="0.0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="2">
                <ValueType Name="double"/>
                <DefaultValue Value="0.0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="3">
                <ValueType Name="double"/>
                <DefaultValue Value="0.0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="4">
                <ValueType Name="double"/>
                <DefaultValue Value="0.0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="5">
                <ValueType Name="double"/>
                <DefaultValue Value="0.0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="6">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
            </MultiValue>
	  </InformationValue>
      
	  <InformationValue TagName="MaterialTran" UIName="TRAN:">
            <Group Name="Spectral"/>
            <MultiValue>
              <InputValue Index="1">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="2">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="3">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="4">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="5">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="6">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
            </MultiValue>
	  </InformationValue>
      
	  <InformationValue TagName="MaterialEmit"  UIName="EMIT:">
            <Group Name="Spectral"/>
            <MultiValue>
              <InputValue Index="1">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="2">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="3">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="4">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="5">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
              <InputValue Index="6">
                <ValueType Name="double"/>
                <DefaultValue Value="0"/>
                <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
              </InputValue>
              
            </MultiValue>
	  </InformationValue>
      
	  <InformationValue TagName="MaterialLeafSize" UIName="Leaf Size:">
            <Group Name="Veg Model"/>
            <ValueType Name="double"/>
            <DefaultValue Value="0.01"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
	  </InformationValue>
      
        </Instance>
      </Templates>
    </TopLevel>
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <!-- #################### Domain #################### -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <TopLevel Section="Domain" UIName="Domain">
      <InformationValue TagName="DomainDimension" UIName="Domain Dimension">
        <ValueType Name="int"/>
        <DefaultValue Value="3"/>
        <Constraint Type="Minimum" Inclusive="Yes" Value="2"/>
        <Constraint Type="Maximum" Inclusive="Yes" Value="3"/>
      </InformationValue>
      <Templates>
        <Instance TagName="DomainMaterialAssignment"/>
      </Templates>
    </TopLevel>
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <!-- #################### Veg #################### -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <TopLevel Section="Veg" UIName="Veg">
      <InformationValue TagName="VegNodeFile" UIName="Node file:">
        <ValueType Name="text"/>
        <DefaultValue Value="veg/veg_Temp"/>
      </InformationValue>
      <InformationValue TagName="VegStartTime" UIName="Start Time (dddhhmm):">
        <ValueType Name="text"/>
      </InformationValue>
      <InformationValue TagName="VegEndTime" UIName="End Time (dddhhmm):">
        <ValueType Name="text"/>
      </InformationValue>
      <InformationValue TagName="METWindHeight" UIName="MET Wind Height:">
        <ValueType Name="double"/>
      </InformationValue>
      <InformationValue TagName="VegOutputMesh" UIName="Output Mesh:">
        <ValueType Name="text"/>
        <DefaultValue Value="veg.2dm"/>
      </InformationValue>
      <InformationValue TagName="OutputEnsightMesh" UIName="Ensight Mesh File Name:">
        <ValueType Name="text"/>
        <Option Default="OFF"/>
      </InformationValue>
      <InformationValue TagName="OutputEnsightNodeFile" UIName="Ensight Node File Name:">
        <ValueType Name="text"/>
        <Option Default="OFF"/>
      </InformationValue>
      <InformationValue TagName="InputFluxFile" UIName="Input Flux File:">
        <ValueType Name="int"/>
        <Advanced/>
        <DefaultValue Value="500"/>
      </InformationValue>
      <Templates>
        <Instance TagName="VegMaterialAssignment">
          <InformationValue TagName="RayVegTwins" UIName="Twins for veg">
            <ValueType Name="void"/>
            <Option Default="OFF"/>
          </InformationValue>
        </Instance>
      </Templates>
    </TopLevel>
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <!-- #################### Analysis #################### -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <TopLevel Section="Analysis" UIName="Analysis">
      <Group Name="Groundwater Flow" Active="1">
      </Group>
      <Group Name="Heat Transport" Active="1">
      </Group>
      <Group Name="Veg Model" Active="0">
      </Group>
      <Group Name="Ray Caster" Active="0">
      </Group>
      <Templates>
	<!-- groundwater flow bcs -->
	<Instance Name="Specified head">
	  <InformationValue TagName="SpecifiedHead" UIName="Specified head:">
	    <Group Name="Groundwater Flow"/>
	    <BoundaryConditionGroup Name="Nodal"/>
	    <ValueType Name="FunctionId"/>
	  </InformationValue>
	</Instance>
	<Instance Name="Specified flux">
	  <InformationValue TagName="SpecifiedFlux" UIName="Specified flux:">
	    <Group Name="Groundwater Flow"/>
	    <BoundaryConditionGroup Name="Boundary"/>
	    <ValueType Name="FunctionId"/>
	  </InformationValue>
	</Instance>
	<Instance Name="Injection well for flow" >
	  <InformationValue TagName="FlowInjectionWell" UIName="Injection well for flow:">
	    <BoundaryConditionGroup Name="Nodal"/>
	    <Group Name="Groundwater Flow"/>
            <Advanced/>
	    <ValueType Name="FunctionId"/>
	  </InformationValue>
	</Instance>
	<Instance Name="MET data">
	  <InformationValue TagName="METData" UIName="Use MET data to drive rainfall:">
	    <BoundaryConditionGroup Name="Boundary"/>
	    <Group Name="Groundwater Flow"/>
            <Advanced/>
	    <ValueType Name="void"/>
	  </InformationValue>
	</Instance>
	<!-- heat transport bcs -->
	<Instance Name="Heat Flux on the ground surface">
	  <InformationValue TagName="GroundSurfaceHeatFlux" UIName="Heat flux on the ground surface:">
	    <BoundaryConditionGroup Name="Boundary"/>
	    <Group Name="Heat Transport"/>
	    <ValueType Name="FunctionId"/>
	  </InformationValue>
	</Instance>
	<Instance Name="Use MET data">
	  <InformationValue TagName="METData" UIName="Use MET data to drive surface heat exchange:">
	    <BoundaryConditionGroup Name="Boundary"/>
            <Group Name="Heat Transport"/>
            <Advanced/>
	    <ValueType Name="void"/>
	  </InformationValue>
	</Instance>
	<Instance Name="Use external ray caster">
	  <InformationValue TagName="RayCaster"  UIName="Use an external ray caster for heat flux:">
	    <BoundaryConditionGroup Name="Boundary"/>
	    <Group Name="Heat Transport"/>
            <Advanced/>
	    <MultiValue>
	      <InputValue Index="1">
		<ValueType Name="int"/>
		<Constraint Type="Minimum" Inclusive="No" Value="0"/>
	      </InputValue>
	      <InputValue Index="2">
		<ValueType Name="int"/>
		<Constraint Type="Minimum" Inclusive="No" Value="0"/>
	      </InputValue>
	    </MultiValue>
	  </InformationValue>
	</Instance>
	<Instance Name="Bottom boundary temperature">
	  <InformationValue TagName="BottomBoundaryTemp" UIName="Bottom boundary temperature:">
	    <BoundaryConditionGroup Name="Nodal"/>
	    <Group Name="Heat Transport"/>
	    <ValueType Name="FunctionId"/>
	  </InformationValue>
	</Instance>
      </Templates>
    </TopLevel>
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <!-- #################### Ray #################### -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <TopLevel Section="Ray" UIName="Ray">
      <InformationValue TagName="RayQCFileName" UIName="Pre-QC file name:">
        <ValueType Name="text"/>
        <DefaultValue Value="rt/*.bin"/>
      </InformationValue>
      
      <InformationValue TagName="RaySoilFlip" UIName="Flip ordering of soil elements' nodes">
        <ValueType Name="void"/>
        <Option Default="OFF"/>
      </InformationValue>
      
      <InformationValue TagName="RayVegFlip" UIName="Flip ordering of veg elements' nodes">
        <ValueType Name="void"/>
        <Option Default="OFF"/>
      </InformationValue>
      
    </TopLevel>
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <!-- #################### Time #################### -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <TopLevel Section="Time" UIName="Time">
      <!-- Add in separator line for formatting ?? -->
      <!-- time control -->
      
      <InformationValue TagName="StartTime" UIName="Start Time:         Units:">
        <Group Name="General"/>
        <!--<ValueType Name="int"/>
        <DefaultValue Value="200"/>
        <Advanced/>-->
        
        <MultiValue>
          <InputValue Index="1">
            <ValueType Name="double"/>
            <DefaultValue Value="200"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
          </InputValue>
          <InputValue Index="2">
            <ValueType Name="int"/>
            <Constraint Type="Discrete" DefaultIndex="0">
              <Enum Value="0" Name="Seconds"/>
              <Enum Value="1" Name="Minutes"/>
              <Enum Value="2" Name="Hours"/>
              <Enum Value="3" Name="Days"/>
              <Enum Value="4" Name="Weeks"/>
              <Enum Value="5" Name="Months"/>
              <Enum Value="6" Name="Years"/>
            </Constraint>
          </InputValue>
        </MultiValue>
      </InformationValue>
      
      <InformationValue TagName="EndTime" UIName="End Time:          Units:">
        <Group Name="General"/>
        <!--<ValueType Name="int"/>
        <DefaultValue Value="200"/>
        <Advanced/>-->

        <MultiValue>
          <InputValue Index="1">
            <ValueType Name="double"/>
	    <DefaultValue Value="200"/>
            <Constraint Type="Minimum" Inclusive="Yes" Value="0"/>
          </InputValue>
          
          <InputValue Index="2">
            <ValueType Name="int"/>
            <Constraint Type="Discrete" DefaultIndex="0">
              <Enum Value="0" Name="Seconds"/>
              <Enum Value="1" Name="Minutes"/>
              <Enum Value="2" Name="Hours"/>
              <Enum Value="3" Name="Days"/>
              <Enum Value="4" Name="Weeks"/>
              <Enum Value="5" Name="Months"/>
              <Enum Value="6" Name="Years"/>
            </Constraint>
          </InputValue>
        </MultiValue>
        
      </InformationValue>
      <InformationValue TagName="TimestepSize" UIName="Timestep Size:&#10;Reference to the xy series">
        <Description Value="Index to the desired time step size series"/>
        <Group Name="General"/>
        <ValueType Name="FunctionId"/>
        <Advanced/>
      </InformationValue>
      
      <InformationValue TagName="AdaptiveTimeStep" UIName="Use Adaptive Time Stepping">
        <!-- defined if it does not exist -->
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="void"/>
        <Option Default="OFF"/>
      </InformationValue>
      
      <!-- output control -->
      <InformationValue TagName="PrintTimeSeriesIndex" UIName="Output time levels:&#10;Pick the xy series that defines what time levels are output.">
        <Group Name="General"/>
        <ValueType Name="FunctionId"/>
        <Advanced/>
      </InformationValue>
      
      <InformationValue TagName="SpecifiedPrintInterval" UIName="Print at specified interval:&#10;Use the OS card to print at a given interval instead of using the xy series.">
        <Group Name="General"/>
        <Advanced/>
        <DefaultValue Value="1.0"/>
        <ValueType Name="double"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
        <Option Default="OFF"/>
      </InformationValue>
      
      <InformationValue TagName="PrintAdaptedMeshes" UIName="Print adapted meshes">
        <Group Name="General"/>
        <ValueType Name="void"/>
        <Advanced/>
        <Option Default="OFF"/>
      </InformationValue>
    </TopLevel>
    
    <!-- ###################################################################### -->   
    <!-- ###################################################################### -->
    <!-- ##################### Globals #################### -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <TopLevel Section="Globals" UIName="Globals">
      <InformationValue TagName="MetFileName" UIName="Met File:">
        <Group Name="General"/>
        <ValueType Name="text"/>
        <DefaultValue Value=".met"/>
        <Advanced/>
      </InformationValue>
      
      <InformationValue TagName="RayToADHSocket" UIName="Socket number from ray caster to ADH,&#10; (fluxes):">
        <Group Name="General"/>
        <ValueType Name="int"/>
        <DefaultValue Value="200"/>
        <Advanced/>
      </InformationValue>
      
      <InformationValue TagName="RayToVegSocket" UIName="Socket number from ray caster to veg, &#10;(fluxes):">
        <Group Name="General"/>
        <ValueType Name="int"/>
        <DefaultValue Value="400"/>
        <Advanced/>
      </InformationValue>
      
      <InformationValue TagName="ADHToRaySocket" UIName="Socket number from ADH to ray caster, &#10;(temperature):">
        <Group Name="General"/>
        <ValueType Name="int"/>
        <DefaultValue Value="300"/>
        <Advanced/>
      </InformationValue>
      
      <InformationValue TagName="VegToRaySocket" UIName="Socket number from veg to ray caster, &#10;(temperature):">
        <Group Name="General"/>
        <ValueType Name="int"/>
        <DefaultValue Value="500"/>
        <Advanced/>
      </InformationValue>
      
      <InformationValue TagName="Gravity" UIName="Gravity:" Units="m/hr^2">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.272024e08"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="WaterSpecificHeat" UIName="Specific Heat of Water:&#10;Taken from water at 25 C:" Units="J/g-C">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="4.1813"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="AirSpecificHeat" UIName="Specific Heat of Air:" Units="J/g-C">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.0035"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="WaterSpecificGravity" UIName="Ratio of water density to the &#10;reference density:">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="1"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="GasSpecificGravity" UIName="Ratio of gas density to reference density:">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="0.001"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="WaterThermalConductivity" UIName="Thermal Conductivity of Water:" Units="W/m K">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="0.58"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="AirThermalConductivity" UIName="Thermal Conductivity of Air:" Units="W/mK">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="0.024"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="ReferenceDensity" UIName="Reference Density, (g/m^3):">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.0e06"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="ReferenceViscosity" UIName="Reference Viscosity, (units ?):">
        <Option Default="ON"/>
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.00E-05"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
    </TopLevel>
    
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <!-- #################### Solvers #################### -->
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
    <TopLevel Section="Solvers" UIName="Solvers">
      <InformationValue TagName="MaxNonLinearIters" UIName="Maximum number of non-linear iterations per timestep:">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="int"/>
        <DefaultValue Value="10"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="NonLinearTolMaxNorm" UIName="Non-linear tolerance (absolute):">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.0E-10"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="NonLinearTolMaxChange" UIName="Non-linear tolerance (increment):">
        <Option Default="ON"/>
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="double"/>
        <DefaultValue Value="1.0E-5"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="MaxLinearIters" UIName="Maximum number of linear iterations per non-linear iteration:">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="int"/>
        <DefaultValue Value="500"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
      </InformationValue>
      
      <InformationValue TagName="PreconditioningBlocks" UIName="Number of Block-Jacobi blocks per processor:&#10;Used for some preconditioners">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="int"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
        <DefaultValue Value="8"/>
      </InformationValue>
      
      <InformationValue TagName="PreconditionerType" UIName="Preconditioner Type:">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="int"/>
        <Constraint Type="Discrete" DefaultIndex="0">
          <Enum Value="0" Name="Point Jacobi"/>
          <Enum Value="1" Name="Block Jacobi"/>
          <Enum Value="2" Name="Additive Schwartz"/>
          <Enum Value="10" Name="LAPACK Dense Direct Solve"/>
          <Enum Value="31" Name="UMFPACK v3.x"/>
          <Enum Value="32" Name="UMFPACK v4.x"/>
        </Constraint>
      </InformationValue>
      
      <InformationValue TagName="MemoryIncrementBlockSize" UIName="Block size for memory increment:">
        <Group Name="General"/>
        <Advanced/>
        <ValueType Name="int"/>
        <Constraint Type="Minimum" Inclusive="No" Value="0"/>
        <DefaultValue Value="8000"/>
      </InformationValue>
    </TopLevel>
    
    <!-- ###################################################################### -->
    <!-- ###################################################################### -->
  </TopLevelGroup>

  <Libraries>
    <FileName Name="FunctionLibraryV2.sbi"/>
    <FileName Name="MaterialLibraryV2.sbi"/>
  </Libraries>

</SimBuilder>

