<?xml version="1.0"?>
<!--Created by XmlV2StringWriter-->
<SMTK_AttributeSystem Version="2">
  <!--**********  Category and Analysis Information ***********-->
  <Categories>
    <Cat>Mechanics</Cat>
  </Categories>
  <Analyses>
    <Analysis Type="Solid Mechanics">
      <Cat>Mechanics</Cat>
      <Cat>Time</Cat>
    </Analysis>
  </Analyses>
  <!--**********  Attribute Definitions ***********-->
  <Definitions>
    <AttDef Type="AdaptationDefinition" Label="AdaptationDefinition" BaseType="" Version="0" Unique="false">
      <ItemDefinitions>
        <String Name="Method" Label="Adaptation Method" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>Unif Size</DefaultValue>
        </String>
        <Int Name="RemeshStepNumber" Label="Remesh Step Number" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>3</DefaultValue>
        </Int>
        <Int Name="MaxNumberOfSTKAdaptIterations" Label="Max Number of STK Adapt Iterations" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>10</DefaultValue>
        </Int>
        <String Name="RefinerPattern" Label="Refiner Pattern" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>Local_Tet4_Tet4_N</DefaultValue>
        </String>
        <Double Name="TargetElementSize" Label="Target Element Size" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0.00050000000000000001</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BelosAlgorithm" Label="BelosAlgorithm" BaseType="" Version="0" Abstract="true" Unique="true">
      <ItemDefinitions>
        <Int Name="AdaptiveBlockSize" Label="Use Adaptive Block Size" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="false">0</Value>
            <Value Enum="true">1</Value>
          </DiscreteInfo>
        </Int>
        <Int Name="BlockSize" Label="Block Size" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="false">0</Value>
            <Value Enum="true">1</Value>
          </DiscreteInfo>
        </Int>
        <Double Name="ConvergenceTolerance" Label="Convergence Tolerance" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1e-08</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Int Name="MaximumIterations" Label="Maximum Iterations" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
        <String Name="Orthogonalization" Label="Orthogonalization Method" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="DGKS">DGKS</Value>
            <Value Enum="ICGS">ICGS</Value>
            <Value Enum="IMGS">IMGS</Value>
          </DiscreteInfo>
        </String>
        <Int Name="OutputFrequency" Label="Output Frequency" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>-1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">-2</Min>
          </RangeInfo>
        </Int>
        <Int Name="OutputStyle" Label="Output Style" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="General">0</Value>
            <Value Enum="Brief">0</Value>
          </DiscreteInfo>
        </Int>
        <Void Name="ShowMaximumResidualNormOnly" Label="Show Maximum Residual Only" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </Void>
        <String Name="TimerLabel" Label="String prefix for timer labels" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>Belos</DefaultValue>
        </String>
        <Int Name="Verbosity" Label="Verbosity" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BelosBlockCGAlgorithm" Label="Belos Block CG Algorithm" BaseType="BelosAlgorithm" Version="0" Unique="true" />
    <AttDef Type="BelosBlockGMRESAlgorithm" Label="Belos Block GMRES Algorithm" BaseType="BelosAlgorithm" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="ExplicitResidualScaling" Label="Explicit Residual Scaling" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>Norm of Initial Residual</DefaultValue>
        </String>
        <String Name="ImplicitResidualScaling" Label="Implicit Residual Scaling" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>Norm of Preconditioned Initial Residual</DefaultValue>
        </String>
        <Void Name="ExplicitResidualTest" Label="Use Explicit Residual Test" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </Void>
        <Void Name="Flexible GMRES" Label="Use Flexible GMRES" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </Void>
        <Int Name="MaximumRestarts" Label="Maximum Restarts" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>20</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="NumBlocks" Label="Maximum Number of Blocks" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>300</DefaultValue>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BelosPseudoBlockGMRESAlgorithm" Label="Belos Pseudo-Block GMRES" BaseType="BelosBlockGMRESAlgorithm" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="DeflationQuorum" Label="Deflation Quorum" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BelosBlockGCRODRAlgorithm" Label="Belos Block GCRODR" BaseType="BelosAlgorithm" Version="0" Unique="true" />
    <AttDef Type="BelosPseudoBlockCGAlgorithm" Label="Belos Pseudo-Block CG" BaseType="BelosAlgorithm" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="DeflationQuorum" Label="Deflation Quorum" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BoundaryCondition" Label="BoundaryCondition" BaseType="" Version="0" Abstract="true" Unique="false">
      <AssociationsDef Name="BoundaryConditionAssociations" Label="BoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="XBoundaryCondition" Label="XBoundaryCondition" BaseType="BoundaryCondition" Version="0" Abstract="true" Unique="true">
      <AssociationsDef Name="XBoundaryConditionAssociations" Label="XBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="StaticDOFXDirichlet" Label="Static DOF X Dirichlet" BaseType="XBoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="StaticDOFXDirichletAssociations" Label="StaticDOFXDirichletAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Dirichlet for DOF X" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="DynamicDOFXDirichlet" Label="Dynamic DOF X Dirichlet" BaseType="XBoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="DynamicDOFXDirichletAssociations" Label="DynamicDOFXDirichletAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Time Dependent Dirichlet for DOF X" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="YBoundaryCondition" Label="YBoundaryCondition" BaseType="BoundaryCondition" Version="0" Abstract="true" Unique="true">
      <AssociationsDef Name="YBoundaryConditionAssociations" Label="YBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="StaticDOFYDirichlet" Label="Static DOF Y Dirichlet" BaseType="YBoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="StaticDOFYDirichletAssociations" Label="StaticDOFYDirichletAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Dirichlet for DOF Y" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="DynamicDOFYDirichlet" Label="Dynamic DOF Y Dirichlet" BaseType="YBoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="DynamicDOFYDirichletAssociations" Label="DynamicDOFYDirichletAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Time Dependent Dirichlet for DOF Y" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ZBoundaryCondition" Label="ZBoundaryCondition" BaseType="BoundaryCondition" Version="0" Abstract="true" Unique="true">
      <AssociationsDef Name="ZBoundaryConditionAssociations" Label="ZBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="StaticDOFZDirichlet" Label="Static DOF Z Dirichlet" BaseType="ZBoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="StaticDOFZDirichletAssociations" Label="StaticDOFZDirichletAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Dirichlet for DOF Z" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="DynamicDOFZDirichlet" Label="Dynamic DOF Z Dirichlet" BaseType="ZBoundaryCondition" Version="0" Unique="true" Nodal="true">
      <AssociationsDef Name="DynamicDOFZDirichletAssociations" Label="DynamicDOFZDirichletAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Value" Label="Time Dependent Dirichlet for DOF Z" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ContinuationSolver" Label="Continuation Solver" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="Stepper" Label="Stepper" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="ContinuationType" Label="Continuation Type" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="Natural">Natural</Value>
                <Value Enum="Arc Length">Arc Length</Value>
              </DiscreteInfo>
            </String>
            <String Name="ContinuationParameter" Label="Continuation Parameter" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
            </String>
            <Double Name="InitialValue" Label="Initial Value" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
            </Double>
            <Double Name="MaxValue" Label="Maximum Value" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
            </Double>
            <Double Name="MinValue" Label="Minimum Value" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
            </Double>
            <Int Name="MaxSteps" Label="Max Steps" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>100</DefaultValue>
            </Int>
            <Int Name="MaxNonlinearIterations" Label="Max Nonlinear Iterations" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>15</DefaultValue>
            </Int>
            <Int Name="SkipParametersDerivative" Label="Skip Parameters Derivative" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="EnableArcLengthScaling" Label="Enable Arc Length Scaling" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <Double Name="GoalArcLengthParameterContribution" Label="Goal Arc Length Parameter Contribution" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.5</DefaultValue>
            </Double>
            <Double Name="MaxArcLengthParameterContribution" Label="Max Arc Length Parameter Contribution" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.80000000000000004</DefaultValue>
            </Double>
            <Double Name="InitialScaleFactor" Label="Initial Scale Factor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Double>
            <Double Name="MinScaleFactor" Label="Min Scale Factor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Int Name="EnableTangentFactorStepSizeScaling" Label="Enable Tangent Factor Step Size Scaling" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <Double Name="MinTangentFactor" Label="Min Tangent Factor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.10000000000000001</DefaultValue>
            </Double>
            <Double Name="TangentFactorExponent" Label="Tangent Factor Exponent" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Double>
            <String Name="BorderedSolverMethod" Label="BorderedSolverMethod" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Bordering">Bordering</Value>
                <Value Enum="Nested">Nested</Value>
              </DiscreteInfo>
            </String>
            <Int Name="ComputeEigenvalues" Label="Compute EigenValues" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <String Name="Eigensolver" Label="Eigensolver" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Default">Default</Value>
                <Value Enum="Anasazi">Anasazi</Value>
              </DiscreteInfo>
            </String>
            <String Name="AnasaziOperator" Label="Anasazi Operator" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Jacobian Inverse">Jacobian Inverse</Value>
                <Value Enum="Shift-Invert (ACB-needs value)">Shift-Invert (ACB-needs value)</Value>
                <Value Enum="Cayley (ACB-needs 2 values)">Cayley (ACB-needs 2 values)</Value>
              </DiscreteInfo>
            </String>
            <Int Name="AnasaziBlockSize" Label="Anasazi Block Size" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Int>
            <Int Name="AnasaziNumBlocks" Label="Anasazi Num Blocks" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>30</DefaultValue>
            </Int>
            <Int Name="AnasaziNumEigenvalues" Label="Anasazi Num Eigenvalues" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>4</DefaultValue>
            </Int>
            <Double Name="AnasaziConvergenceTolerance" Label="Anasazi Convergence Tolerance" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>9.9999999999999995e-08</DefaultValue>
            </Double>
            <Double Name="AnasaziLinearSolveTolerance" Label="Anasazi Linear Solve Tolerance (ACB-default is same value as AnasaziConvergenceTolerance" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>9.9999999999999995e-08</DefaultValue>
            </Double>
            <Int Name="AnasaziStepSize" Label="Anasazi Step Size" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Int>
            <Int Name="AnasaziMaximumRestarts" Label="Anasazi Maximum Restarts" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Int>
            <Int Name="AnasaziSymmetric" Label="Anasazi Symmetric" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="AnasaziNormalizeEigenvectorsWithMassMatrix" Label="Anasazi Normalize Eigenvectors With Mass Matrix" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <String Name="AnasaziVerbosity" Label="Anasazi Verbosity" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>Anasazi::Errors+Anasazi::Warnings+Anasazi::FinalSummary</DefaultValue>
            </String>
            <String Name="AnasaziSortingOrder" Label="Anasazi Sorting Order" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Largest magnitude">LM</Value>
                <Value Enum="Largest real component">LR</Value>
                <Value Enum="Largest imaginary component">LI</Value>
                <Value Enum="Smallest magnitude">SM</Value>
                <Value Enum="Smallest real component">SR</Value>
                <Value Enum="Smallest imaginary component">SI</Value>
                <Value Enum="Largest real part of inverse Cayley transformation">CA</Value>
              </DiscreteInfo>
            </String>
          </ItemDefinitions>
        </Group>
        <AttributeRef Name="BifurcationCalculation" Label="Bifurcation Calculation" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <AttDef>LOCABifurcationAlgorithm</AttDef>
        </AttributeRef>
        <Group Name="Predictor" Label="Predictor" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="Method" Label="Method" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="Constant">Constant</Value>
                <Value Enum="Secant">Secant</Value>
                <Value Enum="Tangent">Tangent</Value>
                <Value Enum="Random">Random</Value>
              </DiscreteInfo>
            </String>
            <Double Name="Epsilon" Label="Epsilon" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="StepSize" Label="Step Size" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="Method" Label="Method" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="Constant">Constant</Value>
                <Value Enum="Adaptive">Adaptive</Value>
              </DiscreteInfo>
            </String>
            <Double Name="InitialStepSize" Label="Initial Step Size" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Double>
            <Double Name="MinStepSize" Label="Min Step Size" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>9.9999999999999998e-13</DefaultValue>
            </Double>
            <Double Name="MaxStepSize" Label="Max Step Size" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1000000000000</DefaultValue>
            </Double>
            <Double Name="FailedStepReductionFactor" Label="FailedStepReductionFactor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.5</DefaultValue>
            </Double>
            <Double Name="SuccessfulStepIncreaseFactor" Label="Successful Step Increase Factor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1.26</DefaultValue>
            </Double>
            <Double Name="Aggressiveness" Label="Aggressiveness" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.5</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Discretization" Label="Discretization" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="WorksetSize" Label="Workset Size" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="CubatureDegree" Label="Cubature Degree" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>3</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="SeparateEvaluatorsByElementBlock" Label="Separate Evaluators By Element Block" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="false">0</Value>
            <Value Enum="true">1</Value>
          </DiscreteInfo>
        </Int>
        <String Name="ExodusInputFileName" Label="Exodus Input File Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </String>
        <String Name="ExodusOutputFileName" Label="Exodus Output File Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </String>
        <String Name="ExodusSolutionName" Label="Exodus Solution Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </String>
        <String Name="ExodusResidualName" Label="Exodus Residual Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </String>
        <Int Name="UseSerialMeshType" Label="Use Serial Mesh Type" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="false">0</Value>
            <Value Enum="true">1</Value>
          </DiscreteInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="LOCABifurcationAlgorithm" Label="LOCABifurcationAlgorithm" BaseType="" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="LinearSolver" Label="LinearSolver" BaseType="" Version="0" Abstract="true" Unique="true">
      <ItemDefinitions>
        <Int Name="EnableDelayedSolverConstruction" Label="Enable delayed solver construction" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="false">0</Value>
            <Value Enum="true">1</Value>
          </DiscreteInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="AmesosLinearSolver" Label="Amesos Linear Solver" BaseType="LinearSolver" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="RefactorizationPolicy" Label="Refactorization policy" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>RepivotOnRefactorization</DefaultValue>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="AztecOOSolver" Label="AxtecOO Solver" BaseType="LinearSolver" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="OutputEveryRHS" Label="Output every RHS" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="false">0</Value>
            <Value Enum="true">1</Value>
          </DiscreteInfo>
        </Int>
        <Void Name="AdjointSolve" Label="Adjoint Solve" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </Void>
        <Int Name="MaximumIterations" Label="Maximum Iterations" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>400</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Double Name="Tolerance" Label="Tolerance" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>9.9999999999999995e-07</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <String Name="SolverAlgorithm" Label="Solver Algorithm" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="CG">CG</Value>
            <Value Enum="GMRES">GMRES</Value>
            <Value Enum="CGS">CGS</Value>
            <Value Enum="TFQMR">TFQMR</Value>
            <Value Enum="BiCGStab">BiCGStab</Value>
            <Value Enum="LU">LU</Value>
          </DiscreteInfo>
        </String>
        <String Name="ConvergenceTest" Label="Convergence Test" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="r0">r0</Value>
            <Value Enum="rhs">rhs</Value>
            <Value Enum="Anorm">Anorm</Value>
            <Value Enum="no scaling">no scaling</Value>
            <Value Enum="sol">sol</Value>
          </DiscreteInfo>
        </String>
        <Int Name="OutputFrequency" Label="Output Frequency" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <String Name="VerbosityLevel" Label="Verbosity Level" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="default">default</Value>
            <Value Enum="none">none</Value>
            <Value Enum="low">low</Value>
            <Value Enum="medium">medium</Value>
            <Value Enum="high">high</Value>
            <Value Enum="extreme">extreme</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BelosLinearSolver" Label="Belos Linear Solver" BaseType="LinearSolver" Version="0" Unique="true">
      <ItemDefinitions>
        <AttributeRef Name="BelosAlgorithm" Label="Solver Algorithm" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <AttDef>BelosAlgorithm</AttDef>
        </AttributeRef>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Material" Label="Material" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="MaterialAssociations" Label="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="ElasticModulus" Label="Elastic Modulus" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="PoissonsRatio" Label="Poisson's Ratio" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
            <Max Inclusive="true">0.5</Max>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="NeohookeanMaterial" Label="Neohookean Material" BaseType="Material" Version="0" Unique="true">
      <AssociationsDef Name="NeohookeanMaterialAssociations" Label="NeohookeanMaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="J2Material" Label="J2 Material" BaseType="Material" Version="0" Unique="true">
      <AssociationsDef Name="J2MaterialAssociations" Label="J2MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="YieldStrength" Label="Yield Strength" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="HardeningModulus" Label="Hardening Modulus" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="SaturationModulus" Label="Saturation Modulus" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </Double>
        <Double Name="SaturationExponent" Label="Saturation Exponent" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="NOXDirection" Label="NOXDirection" BaseType="" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="NOXDirectionNewton" Label="NOX Newton Direction" BaseType="NOXDirection" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="ForcingTermMethod" Label="Forcing Term Method" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Constant">Constant</Value>
            <Value Enum="Type 1">Type 1</Value>
            <Value Enum="Type 2">Type 2</Value>
          </DiscreteInfo>
        </String>
        <Double Name="ForcingTermInitialTolerance" Label="Forcing Term Initial Tolerance" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0.10000000000000001</DefaultValue>
        </Double>
        <Double Name="ForcingTermMinimumTolerance" Label="Forcing Term Minimum Tolerance" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1.0000000000000001e-05</DefaultValue>
        </Double>
        <Double Name="ForcingTermMaximumTolerance" Label="Forcing Term Maximum Tolerance" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0.01</DefaultValue>
        </Double>
        <Double Name="ForcingTermAlpha" Label="Forcing Term Alpha (used only by Type 2)" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1.5</DefaultValue>
        </Double>
        <Double Name="ForcingTermGamma" Label="Forcing Term Gamma (used only by Type 2)" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0.90000000000000002</DefaultValue>
        </Double>
        <Double Name="LinearSolverTolerance" Label="Linear Solver Tolerance" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1.0000000000000001e-09</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="NOXDirectionSteepestDescent" Label="NOX Steepest Descent Direction" BaseType="NOXDirection" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="ScalingType" Label="ScalingType" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="2-Norm">2-Norm</Value>
            <Value Enum="Quadratic Model Min">Quadratic Model Min</Value>
            <Value Enum="F 2-Norm">F 2-Norm</Value>
            <Value Enum="None">None</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="NOXDirectionNonlinearCG" Label="NOX Nonlinear CG Direction" BaseType="NOXDirection" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="Orthogonalize" Label="Orthogonalize" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Fletcher-Reeves">Fletcher-Reeves</Value>
            <Value Enum="Polar-Ribiere">Polar-Ribiere</Value>
          </DiscreteInfo>
        </String>
        <String Name="Precondition" Label="Precondition" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Off">Off</Value>
            <Value Enum="On">On</Value>
          </DiscreteInfo>
        </String>
        <Int Name="RestartFrequency" Label="Restart frequency" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Broyden" Label="NOX Broyden Direction" BaseType="NOXDirection" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="RestartFrequency" Label="Restart frequency" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Double Name="MaxConvergenceRate" Label="Max Convergence Rate" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Int Name="Memory" Label="Memory" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <BriefDescription>The maximum number of past updates that can be saved in memory.</BriefDescription>
          <DetailedDescription>The maximum number of past updates that can be saved in memory. Defaults to the value of "Restart Frequency".</DetailedDescription>
          <DefaultValue>10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Double Name="LinearSolverTolerance" Label="Linear solver tolerance" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0.0001</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Tensor" Label="NOX Tensor Direction" BaseType="NOXDirection" Version="0" Unique="true">
      <BriefDescription>Prerelease only</BriefDescription>
    </AttDef>
    <AttDef Type="ModifiedNewton" Label="NOX Modified Newton Direction" BaseType="NOXDirection" Version="0" Unique="true">
      <BriefDescription>Prerelease only</BriefDescription>
    </AttDef>
    <AttDef Type="QuasiNewton" Label="NOX QuasiNewton Direction" BaseType="NOXDirection" Version="0" Unique="true">
      <BriefDescription>Prerelease only</BriefDescription>
    </AttDef>
    <AttDef Type="NonlinearSolver" Label="NonlinearSolver" BaseType="" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="NOX" Label="NOX" BaseType="NonlinearSolver" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="NOXNonlinearSolver" Label="Nonlinear solver type" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Int Name="AllowedRelativeIncrease" Label="Allowed relative increase" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>100</DefaultValue>
            </Int>
            <Double Name="AphaFactor" Label="Alpha factor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.0001</DefaultValue>
            </Double>
            <AttributeRef Name="CauchyDirection" Label="Cauchy search direction method" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <BriefDescription>Used to specify the second direction in dogleg trust regions methods.</BriefDescription>
              <AttDef>NOXDirection</AttDef>
            </AttributeRef>
            <Double Name="ContractionFactor" Label="Contraction factor" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.25</DefaultValue>
            </Double>
            <Double Name="ContractionTriggerRatio" Label="Contraction trigger ratio" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.10000000000000001</DefaultValue>
            </Double>
            <Double Name="DefaultStep" Label="Default step" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Double>
            <Double Name="ExpansionFactor" Label="Expansion factor" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>4</DefaultValue>
            </Double>
            <Double Name="ExpansionTriggerRatio" Label="Expansion trigger ratio" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.75</DefaultValue>
            </Double>
            <Int Name="ForceInterpolation" Label="Force interpolation" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="False">0</Value>
                <Value Enum="True">1</Value>
              </DiscreteInfo>
            </Int>
            <Double Name="FullStep" Label="Full step" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Double>
            <String Name="InnerIterationMethod" Label="Inner iteration method" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Standard Trust Region">Standard Trust Region</Value>
                <Value Enum="Inexact Trust Region">Inexact Trust Region</Value>
              </DiscreteInfo>
            </String>
            <String Name="InterpolationType" Label="Interpolation type" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="3">
                <Value Enum="Quadratic">Quadratic</Value>
                <Value Enum="Quadratic3">Quadratic3</Value>
                <Value Enum="Cubic">Cubic</Value>
              </DiscreteInfo>
            </String>
            <Double Name="MTCurvatureCondition" Label="Curvature condition" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.99990000000000001</DefaultValue>
            </Double>
            <Double Name="MTDefaultStep" Label="Default step" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Double>
            <Double Name="MTIntervalWidth" Label="Interval width" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1.0000000000000001e-15</DefaultValue>
            </Double>
            <Int Name="MTMaxIters" Label="Maximum iterations" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>20</DefaultValue>
            </Int>
            <Double Name="MTMaximumStep" Label="Maximum step" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1000000</DefaultValue>
            </Double>
            <Double Name="MTMinimumStep" Label="Minimum step" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>9.9999999999999998e-13</DefaultValue>
            </Double>
            <Int Name="MTOptimizeSlopeCalculation" Label="Optimize slope calculation" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="False">0</Value>
                <Value Enum="True">1</Value>
              </DiscreteInfo>
            </Int>
            <Double Name="MTSufficientDecrease" Label="Sufficient decrease" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.0001</DefaultValue>
            </Double>
            <String Name="MTSufficientDecreaseCondition" Label="Sufficient decrease condition" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Armiijo-Goldstein">Armijo-Goldstein</Value>
                <Value Enum="Ared/Pred">Ared/Pred</Value>
              </DiscreteInfo>
            </String>
            <Double Name="MaxBoundsFactor" Label="Maximum bounds factor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.5</DefaultValue>
            </Double>
            <Int Name="MaxIters" Label="Maximum iterations" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
            </Int>
            <Int Name="MaximumIterationForIncrease" Label="Maximum iteration for increase" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Int>
            <Double Name="MaximumStep" Label="Maximum step" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Double>
            <Double Name="MaximumTrustRegionRadius" Label="Maximum trust region radius" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>10000000000</DefaultValue>
            </Double>
            <Double Name="MinBoundsFactor" Label="Minimum bounds factor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.10000000000000001</DefaultValue>
            </Double>
            <Double Name="MinimumImprovementRatio" Label="Minimum improvement ratio" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>0.0001</DefaultValue>
            </Double>
            <Double Name="MinimumStep" Label="Minimum step" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>9.9999999999999998e-13</DefaultValue>
            </Double>
            <Double Name="MinimumTrustRegionRadius" Label="Minimum trust region radius" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DefaultValue>9.9999999999999995e-07</DefaultValue>
            </Double>
            <Double Name="RecoveryStep" Label="Recovery step" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <BriefDescription>Defaults to value for "Default step"</BriefDescription>
              <DefaultValue>1</DefaultValue>
            </Double>
            <Double Name="ReductionFactor" Label="Reduction factor" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
            </Double>
            <String Name="SufficientDecreaseCondition" Label="Sufficient decrease condition" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Armiijo-Goldstein">Armijo-Goldstein</Value>
                <Value Enum="Ared/Pred">Ared/Pred</Value>
                <Value Enum="None">None</Value>
              </DiscreteInfo>
            </String>
            <Int Name="UseAredPredRatioCalculation" Label="Use Ared/Pred ratio calculation" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="UseCauchyInNewtonDirection" Label="Use Cauchy in Newton Direction" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="UseCounters" Label="Use counters" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="UseDoglegSegmentMinimization" Label="Use dogleg segment minimization" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="User counters" Label="Use counters" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="False">0</Value>
                <Value Enum="True">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="WriteOutputParameters" Label="Write output parameters" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="false">0</Value>
                <Value Enum="true">1</Value>
              </DiscreteInfo>
            </Int>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="NOX Full Step Line Search">NOXLineaSearchFullStep</Value>
              <Items>
                <Item>FullStep</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="NOX Backtrack Line Search">NOXLineSearchBacktrack</Value>
              <Items>
                <Item>MinimumStep</Item>
                <Item>DefaultStep</Item>
                <Item>RecoveryStep</Item>
                <Item>MaximumStep</Item>
                <Item>MaxIters</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="NOX Polynomial Line Search">NOXLineSearchPolynomial</Value>
              <Items>
                <Item>DefaultStep</Item>
                <Item>MaxIters</Item>
                <Item>MinimumStep</Item>
                <Item>RecoveryStep</Item>
                <Item>InterpolationType</Item>
                <Item>MinBoundsFactor</Item>
                <Item>MaxBoundsFactor</Item>
                <Item>SufficientDecreaseCondition</Item>
                <Item>ForceInterpolation</Item>
                <Item>User counters</Item>
                <Item>MaximumIterationForIncrease</Item>
                <Item>AllowedRelativeIncrease</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="NOX More Thuente Search">NOXLineSearchMoreThuente</Value>
              <Items>
                <Item>MTSufficientDecreaseCondition</Item>
                <Item>MTSufficientDecrease</Item>
                <Item>MTOptimizeSlopeCalculation</Item>
                <Item>MTIntervalWidth</Item>
                <Item>MTMaximumStep</Item>
                <Item>MTMinimumStep</Item>
                <Item>MTDefaultStep</Item>
                <Item>RecoveryStep</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="NOX Trust Region">NOXTrustRegion</Value>
              <Items>
                <Item>RecoveryStep</Item>
                <Item>CauchyDirection</Item>
                <Item>MinimumTrustRegionRadius</Item>
                <Item>MaximumTrustRegionRadius</Item>
                <Item>MinimumImprovementRatio</Item>
                <Item>ContractionTriggerRatio</Item>
                <Item>ContractionFactor</Item>
                <Item>ExpansionTriggerRatio</Item>
                <Item>ExpansionFactor</Item>
                <Item>UseAredPredRatioCalculation</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="NOX InexactTrust Region">NOXInexactTrustRegion</Value>
              <Items>
                <Item>CauchyDirection</Item>
                <Item>InnerIterationMethod</Item>
                <Item>MinimumTrustRegionRadius</Item>
                <Item>MaximumTrustRegionRadius</Item>
                <Item>MinimumImprovementRatio</Item>
                <Item>ContractionTriggerRatio</Item>
                <Item>ContractionFactor</Item>
                <Item>ExpansionTriggerRatio</Item>
                <Item>ExpansionFactor</Item>
                <Item>RecoveryStep</Item>
                <Item>UseAredPredRatioCalculation</Item>
                <Item>UseDoglegSegmentMinimization</Item>
                <Item>UseCounters</Item>
                <Item>WriteOutputParameters</Item>
              </Items>
            </Structure>
            <Value Enum="NOX Tensor">NOXTensor</Value>
          </DiscreteInfo>
        </String>
        <AttributeRef Name="NOXDirection" Label="Primary search direction method" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <AttDef>NOXDirection</AttDef>
        </AttributeRef>
        <String Name="StatusTestCheckType" Label="Status test check type" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Complete">Complete</Value>
            <Value Enum="Minimal">Minimal</Value>
            <Value Enum="None">None</Value>
          </DiscreteInfo>
        </String>
        <Group Name="Printing" Label="Printing" Version="0" NumberOfRequiredGroups="1" />
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Preconditioner" Label="Preconditioner" BaseType="" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="Ifpack" Label="Ifpack" BaseType="Preconditioner" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="PreconditionerType" Label="Preconditioner type" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>"ILU"</DefaultValue>
        </String>
        <Int Name="Overlap" Label="Partitioner overlap" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Double Name="DropTolerance" Label="Drop tolerance" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1.0000000000000001e-09</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="ILUTLevelOfFill" Label="ILUT Level of fill" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Int Name="LevelOfFill" Label="Level of fill" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ProblemDefinition" Label="ProblemDefinition" BaseType="" Version="0" Unique="false">
      <ItemDefinitions>
        <String Name="SolutionMethod" Label="Solution Method" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Steady">Steady</Value>
            <Value Enum="Transient">Transient</Value>
            <Value Enum="Continuation">Continuation</Value>
            <Value Enum="Multi-Problem">Multi-Problem</Value>
          </DiscreteInfo>
        </String>
        <Int Name="PhalanxGraphVisualizationDetails" Label="Phalanx Graph Visualization Detail" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SimExpression" Label="SimExpression" BaseType="" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="SimInterpolation" Label="SimInterpolation" BaseType="SimExpression" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="PolyLinearFunction" Label="Polylinear Function" BaseType="SimInterpolation" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="ValuePairs" Label="Value Pairs" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="X" Label="X" Version="0" NumberOfRequiredValues="0">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
            </Double>
            <Double Name="Value" Label="Value" Version="0" NumberOfRequiredValues="0">
              <Categories>
                <Cat>Mechanics</Cat>
              </Categories>
            </Double>
          </ItemDefinitions>
        </Group>
        <String Name="Sim1DLinearExp" Label="Sim1DLinearExp" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Mechanics</Cat>
          </Categories>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- ** Example of an attribute definition that can be used as a
	 selector ***-->
     <AttDef Type="ViewSelector">
       <ItemDefinitions>
	 <String Name="Selector" Label="View Selection">
           <DiscreteInfo DefaultIndex="0">
             <Value Enum="ViewGroup1">vg1</Value>
             <Value Enum="ViewGroup2">vg2</Value>
             <Value Enum="ViewGroup3">vg3</Value>
             <Value Enum="ViewGroup4">vg4</Value>
           </DiscreteInfo>
         </String>
       </ItemDefinitions>
     </AttDef>
  </Definitions>
  <!--********** Workflow Views ***********-->
  <Views>
    <View Type="Selector" Title="SimBuilder" SelectorName="selectorAtt" SelectorType="ViewSelector" TopLevel="true">
      <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
      <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
      <Views>
        <View Title="Group1" Enum="ViewGroup1"/>
        <View Title="Group2" Enum="ViewGroup2"/>
        <View Title="Group3" Enum="ViewGroup3"/>
      </Views>
    </View>
    <View Type="Group" Title="Group1">
     <Views>
        <View Title="Problem Definition" />
        <View Title="Materials" />
        <View Title="Solvers" />
      </Views>
    </View>
    <View Type="Group" Title="Group2">
     <Views>
       <View Title="Nonlinear Search Direction" />
        <View Title="Belos Algorithm" />
     </Views>
    </View>
    <View Type="Group" Title="Group3">
     <Views>
         <View Title="Boundary Conditions" />
        <View Title="Functions" />
      </Views>
    </View>
    <View Type="Attribute" Title="Belos Algorithm">
      <AttributeTypes>
        <Att Type="BelosAlgorithm" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Boundary Conditions" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Type="BoundaryCondition" />
      </AttributeTypes>
    </View>
    <View Type="SimpleExpression" Title="Functions">
      <Att Type="PolyLinearFunction" />
    </View>
    <View Type="Attribute" Title="Materials" CreateEntities="true" ModelEntityFilter="r">
      <AttributeTypes>
        <Att Type="Material" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Nonlinear Search Direction">
      <AttributeTypes>
        <Att Type="NOXDirection" />
      </AttributeTypes>
    </View>
    <View Type="Instanced" Title="Problem Definition">
      <InstancedAttributes>
        <Att Name="ProblemDefinition" Type="ProblemDefinition" />
        <Att Name="Discretization" Type="Discretization" />
        <Att Name="AdaptationDefinition" Type="AdaptationDefinition" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Solvers">
      <AttributeTypes>
        <Att Type="NonlinearSolver" />
        <Att Type="LinearSolver" />
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeSystem>
