<?xml version="1.0"?>
<!--Created by XmlV2StringWriter-->
<SMTK_AttributeSystem Version="2">
  <!--**********  Category and Analysis Information ***********-->
  <Categories>
    <Cat>Energy Equation</Cat>
    <Cat>General</Cat>
    <Cat>Incompressible Navier-Stokes</Cat>
  </Categories>
  <Analyses>
    <Analysis Type="Incompressible Navier-Stokes Analysis">
      <Cat>General</Cat>
      <Cat>Incompressible Navier-Stokes</Cat>
    </Analysis>
    <Analysis Type="NS and Energy Equation Analysis">
      <Cat>Energy Equation</Cat>
      <Cat>General</Cat>
      <Cat>Incompressible Navier-Stokes</Cat>
    </Analysis>
  </Analyses>
  <!--**********  Attribute Definitions ***********-->
  <Definitions>
    <AttDef Type="BasicTurbulenceModel" Label="Turbulence Model" BaseType="" Version="0" Unique="false">
      <ItemDefinitions>
        <String Name="Method" Label="Turbulence Model" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>(tmodel or turbulence)</BriefDescription>
          <ChildrenDefinitions>
            <Double Name="c_s" Label="Smagorinsky Model Constant" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Incompressible Navier-Stokes</Cat>
              </Categories>
              <BriefDescription>Smagorinsky model constant.</BriefDescription>
              <DefaultValue>0.17999999999999999</DefaultValue>
            </Double>
            <Double Name="c_w" Label="WALE Model Constant" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Incompressible Navier-Stokes</Cat>
              </Categories>
              <BriefDescription>WALE model constant.</BriefDescription>
              <DefaultValue>0.17999999999999999</DefaultValue>
            </Double>
            <Double Name="prandtl" Label="Turbulent Prandtl Number" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Incompressible Navier-Stokes</Cat>
              </Categories>
              <DefaultValue>0.88900000000000001</DefaultValue>
            </Double>
            <Double Name="schmidt" Label="Turbulent Schmidt Number" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Incompressible Navier-Stokes</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
            </Double>
            <Void Name="timescale_limiter" Label="Time Scale Limiter" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
            </Void>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Value Enum="Spalart-Allmaras">spalart_allmaras</Value>
            <Value Enum="Detached Eddy Spalart-Allmaras">spalart_allmaras_des</Value>
            <Structure>
              <Value Enum="Smagorinsky">smagorinsky</Value>
              <Items>
                <Item>c_s</Item>
                <Item>prandtl</Item>
                <Item>schmidt</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="WALE">WALE</Value>
              <Items>
                <Item>c_w</Item>
                <Item>prandtl</Item>
                <Item>schmidt</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="RNG k-e">rng_ke</Value>
              <Items>
                <Item>timescale_limiter</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BodyForce" BaseType="" Version="0" Abstract="true" Unique="false">
      <AssociationsDef Name="BodyForceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|domain|3</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="GravityForce" Label="Gravity Force" BaseType="BodyForce" Version="0" Unique="true">
      <AssociationsDef Name="GravityForceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|domain|3</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="GravityForce" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(body_force::lcid)</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="3">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(body_force::{fx,fy,fz})</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BoussinesqForce" Label="Boussinesq Force" BaseType="BodyForce" Version="0" Unique="true">
      <AssociationsDef Name="BoussinesqForceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|domain|3</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="BoussinesqForce" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(boussinesqforce::lcid)</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="3">
          <Categories>
            <Cat>Energy Equation</Cat>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(boussinesqforce::{gx,gy,gz})</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="porous_drag" Label="Porous Drag" BaseType="BodyForce" Version="0" Unique="true">
      <AssociationsDef Name="porous_dragAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|domain|3</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="porous_drag" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(porous_drag::lcid)</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(porous_drag::amp)</BriefDescription>
          <DefaultValue>1</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="HeatSource" Label="Heat Source" BaseType="BodyForce" Version="0" Unique="true">
      <AssociationsDef Name="HeatSourceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|domain|3</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="HeatSource" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(heat_source::lcid)</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(heat_source::Q)</BriefDescription>
          <DefaultValue>0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="BoundaryCondition" BaseType="" Version="0" Abstract="true" Unique="false">
      <AssociationsDef Name="BoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="EnthalpyBoundaryCondition" Label="Enthalpy" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="EnthalpyBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(enthalpybc::sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(enthalpybc::sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Turbulent Dissipation" Label="Turbulent Dissipation" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="Turbulent DissipationAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>Used with the RNG k-e turbulence model (epsbc::sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>Used with the RNG k-e turbulence model (epsbc::sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="distancebc" Label="Distance Function" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="distancebcAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(distancebc::sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(distancebc::sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Pressure" Label="Pressure" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="PressureAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(pressurebc::sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(pressurebc::sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Temperature" Label="Temperature" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="TemperatureAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(temperaturebc::sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(temperaturebc::sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TurbulentViscosity" Label="Turbulent Viscosity" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="TurbulentViscosityAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>Used with the RNG k-e turbulence model (turbnubc::sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>Used with the RNG k-e turbulence model (turbnubc::sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="VelXBoundaryCondition" Label="X Velocity" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="VelXBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(velocitybc::velx sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(velocitybc::velx sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SymmetryVelXBoundaryCondition" Label="X Symmetry Velocity" BaseType="VelXBoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="SymmetryVelXBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="VelYBoundaryCondition" Label="Y Velocity" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="VelYBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(velocitybc::vely sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(velocitybc::vely sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SymmetryVelYBoundaryCondition" Label="Y Symmetry Velocity" BaseType="VelYBoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="SymmetryVelYBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="VelZBoundaryCondition" Label="Z Velocity" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="VelZBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(velocitybc::velz sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(velocitybc::velz sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SymmetryVelZBoundaryCondition" Label="Z Symmetry Velocity" BaseType="VelZBoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="SymmetryVelZBoundaryConditionAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="HeatFlux" Label="Heat Flux" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="HeatFluxAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="LoadCurve" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(heatflux::sideset [loadCurveId])</BriefDescription>
          <ExpressionType>PolyLinearFunction</ExpressionType>
        </Double>
        <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(heatflux::sideset [amplitude])</BriefDescription>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="passiveoutflowbc" Label="Passive Outflow" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="passiveoutflowbcAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Void Name="void" Label="Not used - do not show" Version="0" AdvanceLevel="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(passiveoutflowbc::sideset)</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="pressureoutflowbc" Label="Pressure Outflow" BaseType="BoundaryCondition" Version="0" Unique="true">
      <AssociationsDef Name="pressureoutflowbcAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Void Name="void" Label="Not used - do not show" Version="0" AdvanceLevel="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(pressureoutflowbc::sideset)</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ExecutionControl" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="ExecutionControl" Label="Execution Control Frequency Checking" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>Frequency of checking execution control file (exe_control)</BriefDescription>
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="InitialConditions" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="InitialConditions" Label="Initial Conditions" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="Velocity" Label="Initial velocity" Version="0" NumberOfRequiredValues="3">
              <Categories>
                <Cat>Incompressible Navier-Stokes</Cat>
              </Categories>
              <BriefDescription>(initial::{velx,vely,velz})</BriefDescription>
              <ComponentLabels>
                <Label>X</Label>
                <Label>Y</Label>
                <Label>Z</Label>
              </ComponentLabels>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="tke" Label="Initial turbulent kinetic energy" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Incompressible Navier-Stokes</Cat>
              </Categories>
              <BriefDescription>Turbulent kinetic energy for k-e and k-w models (initial::tke)</BriefDescription>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="itdr" Label="Initial turbulent dissipation rate" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Incompressible Navier-Stokes</Cat>
              </Categories>
              <BriefDescription>Initial turbulent dissipation rate for k-e models (initial::eps)</BriefDescription>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="idts" Label="Inverse dissipation time scale" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Incompressible Navier-Stokes</Cat>
              </Categories>
              <BriefDescription>Initial turbulent dissipation time scale for k-w models (initial::omega)</BriefDescription>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="tv" Label="Turbulent viscosity" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Incompressible Navier-Stokes</Cat>
              </Categories>
              <BriefDescription>Turbulent viscosity for Spalart-Allmaras and DES models (initial::turbnu)</BriefDescription>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="temperature" Label="Temperature" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Energy Equation</Cat>
              </Categories>
              <BriefDescription>(initial::temperature)</BriefDescription>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="internalenergy" Label="Internal Energy" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Energy Equation</Cat>
              </Categories>
              <BriefDescription>(initial::int_energy)</BriefDescription>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="enthalpy" Label="Enthalpy" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>Energy Equation</Cat>
              </Categories>
              <BriefDescription>(initial::enthalpy)</BriefDescription>
              <DefaultValue>0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="LoadBalancer" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="Method" Label="Load Balance Method" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>(load_balance::method)</BriefDescription>
          <DiscreteInfo DefaultIndex="4">
            <Value Enum="RCB">rcb</Value>
            <Value Enum="RIB">rib</Value>
            <Value Enum="SFC">sfc</Value>
            <Value Enum="Hypergraph">hg</Value>
            <Value Enum="SFC and HG">sfc_and_hg</Value>
          </DiscreteInfo>
        </String>
        <Void Name="Load Balance Diagnostics" Label="Load Balance Diagnostics" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>Load Balance Verbose Level (load_balance::diagnostics)</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Material" Label="Material" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|domain|3</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Density" Label="Density" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>(material::rho)</BriefDescription>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <String Name="specificheattype" Label="Specific Heat Type" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(material::Cp or material::Cv type)</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Constant Pressure">Cp</Value>
            <Value Enum="Constant Volume">Cv</Value>
          </DiscreteInfo>
        </String>
        <Double Name="specificheatvalue" Label="Specific Heat Value" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(material::Cp or material::Cv value)</BriefDescription>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="k" Label="Thermal conductivity" Version="0" NumberOfRequiredValues="6">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>Symmetric thermal conductivity tensor (material::{k11,k12,k13,k22,k23,k33})</BriefDescription>
          <ComponentLabels>
            <Label>11</Label>
            <Label>12</Label>
            <Label>13</Label>
            <Label>22</Label>
            <Label>23</Label>
            <Label>33</Label>
          </ComponentLabels>
        </Double>
        <Double Name="mu" Label="Mu" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Incompressible Navier-Stokes</Cat>
          </Categories>
          <BriefDescription>Molecular viscosity (material::mu)</BriefDescription>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="beta" Label="Beta" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>Coefficient of thermal expansion (material::beta)</BriefDescription>
          <DefaultValue>0</DefaultValue>
        </Double>
        <Double Name="Tref" Label="Material Reference Temperature" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(material::Tref)</BriefDescription>
          <DefaultValue>0</DefaultValue>
        </Double>
        <Void Name="rigid" Label="Rigid body specification" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>Rigid body material specification for native conjugate heat transfer (material::rigid)</BriefDescription>
        </Void>
        <Double Name="velxyz" Label="Rigid body velocity" Version="0" NumberOfRequiredValues="3">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(material::{velx,vely,velz})</BriefDescription>
          <ComponentLabels>
            <Label>1</Label>
            <Label>2</Label>
            <Label>3</Label>
          </ComponentLabels>
          <DefaultValue>0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Output" BaseType="" Version="0" Unique="false">
      <ItemDefinitions>
        <String Name="type" Label="Output Type" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>Output format for restart and field files (filetype)</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Serial">serial</Value>
            <Value Enum="Distributed">distributed</Value>
          </DiscreteInfo>
        </String>
        <Group Name="RestartOutput" Label="Restart Output" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Int Name="frequency" Label="Frequency" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Output dump file frequency. 0 implies do not output. (dump)</BriefDescription>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="FieldOutput" Label="Field Output" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Int Name="frequency" Label="Frequency" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Output plot file frequency. 0 implies do not output. (plti)</BriefDescription>
              <DefaultValue>20</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Int>
            <String Name="type" Label="Type" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Output field format (pltype)</BriefDescription>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="GMV ASCII">gmv_ascii</Value>
                <Value Enum="ExodusII">exodusii</Value>
                <Value Enum="ExodusII (64 bit)">exodusii64</Value>
                <Value Enum="ExodusII (HDF5)">exodusii_hdf5</Value>
                <Value Enum="ExodusII (64 bit HDF5)">exodusii64_hdf5</Value>
                <Value Enum="VTK ASCII">vtk_ascii</Value>
              </DiscreteInfo>
            </String>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SimExpression" BaseType="" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="SimInterpolation" BaseType="SimExpression" Version="0" Abstract="true" Unique="true" />
    <AttDef Type="PolyLinearFunction" Label="Expression" BaseType="SimInterpolation" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="ValuePairs" Label="Value Pairs" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="X" Version="0" NumberOfRequiredValues="0" Extensible="true">
              <Categories>
                <Cat>General</Cat>
              </Categories>
            </Double>
            <Double Name="Value" Version="0" NumberOfRequiredValues="0" Extensible="true">
              <Categories>
                <Cat>General</Cat>
              </Categories>
            </Double>
          </ItemDefinitions>
        </Group>
        <String Name="Sim1DLinearExp" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="StatusInformation" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="minmaxfrequency" Label="Interval to report min/max velocity values" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>(ttyi)</BriefDescription>
          <DefaultValue>10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Int Name="tifrequency" Label="Time history write frequency" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>Interval to write time-history data to the time-history files (thti)</BriefDescription>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <String Name="PrintLevel" Label="ASCII Print Level" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>Controls the amount of data written to the ASCII (human-readable) output file (prtlevel)</BriefDescription>
          <ChildrenDefinitions>
            <Int Name="hcfrequency" Label="ASCII Verbose Print Interval" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(prti)</BriefDescription>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Int>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="param">param</Value>
            <Value Enum="results">results</Value>
            <Structure>
              <Value Enum="verbose">verbose</Value>
              <Items>
                <Item>hcfrequency</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TempStatVarOutput" BaseType="" Version="0" Abstract="true" Unique="false" />
    <AttDef Type="NodeTempStatVarOutput" Label="Node plotstatvar" BaseType="TempStatVarOutput" Version="0" Unique="false">
      <ItemDefinitions>
        <String Name="varname" Label="Variable Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DiscreteInfo>
            <Value Enum="&lt;density&gt;">&lt;density&gt;</Value>
            <Value Enum="&lt;pressure&gt;">&lt;pressure&gt;</Value>
            <Value Enum="&lt;velocity&gt;">&lt;velocity&gt;</Value>
            <Value Enum="&lt;temperature&gt;">&lt;temperature&gt;</Value>
            <Value Enum="&lt;enstrophy&gt;">&lt;enstrophy&gt;</Value>
            <Value Enum="&lt;helicity&gt;">&lt;helicity&gt;</Value>
            <Value Enum="&lt;vorticity&gt;">&lt;vorticity&gt;</Value>
            <Value Enum="&lt;pressure',pressure'&gt;">&lt;pressure',pressure'&gt;</Value>
            <Value Enum="&lt;temp',temp'&gt;">&lt;temp',temp'&gt;</Value>
            <Value Enum="&lt;density',pressure'&gt;">&lt;density',pressure'&gt;</Value>
            <Value Enum="&lt;pressure',velocity'&gt;">&lt;pressure',velocity'&gt;</Value>
            <Value Enum="rms-pressure">rms-pressure</Value>
            <Value Enum="rms-temp">rms-temp</Value>
            <Value Enum="tke">tke</Value>
            <Value Enum="reynoldsstress">reynoldsstress</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ElemTempStatVarOutput" Label="Elem plotstatvar" BaseType="TempStatVarOutput" Version="0" Unique="false">
      <ItemDefinitions>
        <String Name="varname" Label="Variable Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DiscreteInfo>
            <Value Enum="&lt;density&gt;">&lt;density&gt;</Value>
            <Value Enum="&lt;pressure&gt;">&lt;pressure&gt;</Value>
            <Value Enum="&lt;velocity&gt;">&lt;velocity&gt;</Value>
            <Value Enum="&lt;temperature&gt;">&lt;temperature&gt;</Value>
            <Value Enum="&lt;enstrophy&gt;">&lt;enstrophy&gt;</Value>
            <Value Enum="&lt;helicity&gt;">&lt;helicity&gt;</Value>
            <Value Enum="&lt;vorticity&gt;">&lt;vorticity&gt;</Value>
            <Value Enum="&lt;turbnu&gt;">&lt;turbnu&gt;</Value>
            <Value Enum="&lt;pressure',pressure'&gt;">&lt;pressure',pressure'&gt;</Value>
            <Value Enum="&lt;temp',temp'&gt;">&lt;temp',temp'&gt;</Value>
            <Value Enum="&lt;density',pressure'&gt;">&lt;density',pressure'&gt;</Value>
            <Value Enum="&lt;pressure',velocity'&gt;">&lt;pressure',velocity'&gt;</Value>
            <Value Enum="rms-pressure">rms-pressure</Value>
            <Value Enum="rms-temp">rms-temp</Value>
            <Value Enum="tke">tke</Value>
            <Value Enum="reynoldsstress">reynoldsstress</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SideSetTempStatVarOutput" Label="SideSet plotstatvar" BaseType="TempStatVarOutput" Version="0" Unique="false">
      <AssociationsDef Name="SideSetTempStatVarOutputAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="varname" Label="Variable Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DiscreteInfo>
            <Value Enum="&lt;density&gt;">&lt;density&gt;</Value>
            <Value Enum="&lt;pressure&gt;">&lt;pressure&gt;</Value>
            <Value Enum="&lt;velocity&gt;">&lt;velocity&gt;</Value>
            <Value Enum="&lt;temperature&gt;">&lt;temperature&gt;</Value>
            <Value Enum="&lt;heatflux&gt;">&lt;heatflux&gt;</Value>
            <Value Enum="&lt;straction&gt;">&lt;straction&gt;</Value>
            <Value Enum="&lt;ntraction&gt;">&lt;ntraction&gt;</Value>
            <Value Enum="&lt;traction&gt;">&lt;traction&gt;</Value>
            <Value Enum="&lt;pressure',pressure'&gt;">&lt;pressure',pressure'&gt;</Value>
            <Value Enum="&lt;temp',temp'&gt;">&lt;temp',temp'&gt;</Value>
            <Value Enum="&lt;density',pressure'&gt;">&lt;density',pressure'&gt;</Value>
            <Value Enum="&lt;pressure',velocity'&gt;">&lt;pressure',velocity'&gt;</Value>
            <Value Enum="rms-pressure">rms-pressure</Value>
            <Value Enum="rms-temp">rms-temp</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TempStatVarStatistics" Label="TempStatVarStatistics" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="TemporalStatistics" Label="Temporal Statistics" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="StartTime" Label="Start Time" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(statistics::starttime)</BriefDescription>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="EndTime" Label="End Time" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(statistics::endtime)</BriefDescription>
              <DefaultValue>1</DefaultValue>
            </Double>
            <Double Name="PlotWinSize" Label="Time Window Size" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(statistics::plotwinsize)</BriefDescription>
              <DefaultValue>0.10000000000000001</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="VarOutput" BaseType="" Version="0" Abstract="true" Unique="false" />
    <AttDef Type="ElemHistVarOutput" Label="Element Time History Output" BaseType="VarOutput" Version="0" Unique="false">
      <ItemDefinitions>
        <Int Name="Id" Label="Id" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <String Name="varname" Label="Variable Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DiscreteInfo>
            <Value Enum="density">density</Value>
            <Value Enum="div">div</Value>
            <Value Enum="enstrophy">enstrophy</Value>
            <Value Enum="enthalpy">enthalpy</Value>
            <Value Enum="helicity">helicity</Value>
            <Value Enum="pressure">pressure</Value>
            <Value Enum="temp">temp</Value>
            <Value Enum="turbeps">turbeps</Value>
            <Value Enum="turbke">turbke</Value>
            <Value Enum="turbnu">turbnu</Value>
            <Value Enum="vel">vel</Value>
            <Value Enum="vorticity">vorticity</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SideSetHistVarOutput" Label="Sideset Time History Output" BaseType="VarOutput" Version="0" Unique="false">
      <AssociationsDef Name="SideSetHistVarOutputAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="varname" Label="Variable Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DiscreteInfo>
            <Value Enum="avgpress">avgpress</Value>
            <Value Enum="avgtemp">avgtemp</Value>
            <Value Enum="avgvel">avgvel</Value>
            <Value Enum="force">force</Value>
            <Value Enum="fvol">fvol</Value>
            <Value Enum="heatflow">heatflow</Value>
            <Value Enum="massflow">massflow</Value>
            <Value Enum="pressforce">pressforce</Value>
            <Value Enum="surfarea">surfarea</Value>
            <Value Enum="viscforce">viscforce</Value>
            <Value Enum="volumeflow">volumeflow</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="NodePlotVarOutput" Label="Node Variable Output" BaseType="VarOutput" Version="0" Unique="false">
      <ItemDefinitions>
        <String Name="varname" Label="Variable Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DiscreteInfo>
            <Value Enum="density">density</Value>
            <Value Enum="dist">dist</Value>
            <Value Enum="enthalpy">enthalpy</Value>
            <Value Enum="enstrophy">enstrophy</Value>
            <Value Enum="helicity">helicity</Value>
            <Value Enum="pressure">pressure</Value>
            <Value Enum="procid">procid</Value>
            <Value Enum="temp">temp</Value>
            <Value Enum="turbeps">turbeps</Value>
            <Value Enum="turbke">turbke</Value>
            <Value Enum="turbnu">turbnu</Value>
            <Value Enum="u">u</Value>
            <Value Enum="vel">vel</Value>
            <Value Enum="vginv2">vginv2</Value>
            <Value Enum="vorticity">vorticity</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ElemPlotVarOutput" Label="Element Variable Output" BaseType="VarOutput" Version="0" Unique="false">
      <ItemDefinitions>
        <String Name="varname" Label="Variable Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DiscreteInfo>
            <Value Enum="cfl">cfl</Value>
            <Value Enum="density">density</Value>
            <Value Enum="dist">dist</Value>
            <Value Enum="div">div</Value>
            <Value Enum="enthalpy">enthalpy</Value>
            <Value Enum="enstrophy">enstrophy</Value>
            <Value Enum="helicity">helicity</Value>
            <Value Enum="pressure">pressure</Value>
            <Value Enum="procid">procid</Value>
            <Value Enum="temp">temp</Value>
            <Value Enum="turbeps">turbeps</Value>
            <Value Enum="turbke">turbke</Value>
            <Value Enum="turbnu">turbnu</Value>
            <Value Enum="vel">vel</Value>
            <Value Enum="vginv2">vginv2</Value>
            <Value Enum="volume">volume</Value>
            <Value Enum="vorticity">vorticity</Value>
            <Value Enum="ystar">ystar</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SideSetPlotVarOutput" Label="Sideset Variable Output" BaseType="VarOutput" Version="0" Unique="false">
      <AssociationsDef Name="SideSetPlotVarOutputAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <Categories>
          <Cat>General</Cat>
        </Categories>
        <MembershipMask>group|bdy|2</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="varname" Label="Variable Name" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <DiscreteInfo>
            <Value Enum="traction">traction</Value>
            <Value Enum="straction">straction</Value>
            <Value Enum="ntraction">ntraction</Value>
            <Value Enum="wallshear">wallshear</Value>
            <Value Enum="yplus">yplus</Value>
            <Value Enum="ystar">ystar</Value>
            <Value Enum="varyplus">varyplus</Value>
            <Value Enum="heatflux">heatflux</Value>
            <Value Enum="nheatflux">nheatflux</Value>
            <Value Enum="surfarea">surfarea</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="energy" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="energy" Label="Energy Equation Formulation" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Energy Equation</Cat>
          </Categories>
          <BriefDescription>(energy)</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Isothermal">isothermal</Value>
            <Value Enum="Temperature">temperature</Value>
            <Value Enum="Enthalpy">enthalpy</Value>
            <Value Enum="Internal Energy">int_energy</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="hydrostat" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="Hydrostat" Label="Hydrostatic pressure" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="Value" Label="Load Curve" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(hydrostat::nodeset [amplitude])</BriefDescription>
              <DetailedDescription>Prescribe the hydrostatic pressure. This may be used in conjunction with prescribed pressure
              boundary conditions, or by itself. When used by itself, the hstat keyword plays two roles. It makes
              the pressure-Poisson equation non-singular and it permits the pressure for the system to be uniquely
              determined. When the hstat keyword is used with prescribed pressure boundary conditions, then
              it only specifies the unique hydrostatic pressure level for the system. In either case, the pressure
              time-history and field output is adjusted to reflect the specified hydrostatic pressure level.</DetailedDescription>
              <ExpressionType>PolyLinearFunction</ExpressionType>
            </Double>
            <Double Name="Scale" Label="Scale" Version="0" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(hydrostat::nodeset [loadCurveId])</BriefDescription>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="momentumsolver" Label="Momentum Solver" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="MomentumSolver" Label="Momentum Solver" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="type" Label="Type" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(momentumsolver::type)</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Flexible GMRES">FGMRES</Value>
                <Value Enum="ILU-Preconditioned FGMRES">ILUFGMRES</Value>
                <Value Enum="GMRES">GMRES</Value>
                <Value Enum="ILU-Preconditioned GMRES">ILUGMRES</Value>
              </DiscreteInfo>
            </String>
            <Int Name="restart" Label="Number of restart vectors" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(momentumsolver::restart)</BriefDescription>
              <DefaultValue>30</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">100</Max>
              </RangeInfo>
            </Int>
            <Int Name="itmax" Label="Maximum number of iterations" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>The maximum number of iterations (momentumsolver::itmax).</BriefDescription>
              <DefaultValue>500</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">500</Max>
              </RangeInfo>
            </Int>
            <Int Name="itchk" Label="Convergence criteria checking frequency" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>The number of iterations to take before checking convergence criteria (momentumsolver::itchk)</BriefDescription>
              <DefaultValue>2</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">25</Max>
              </RangeInfo>
            </Int>
            <Void Name="diagnostics" Label="Diagnostics" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Enable/disable the diagnostic information from the solver (momentumsolver::diagnostics)</BriefDescription>
            </Void>
            <Void Name="convergence" Label="Convergence metrics" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Enable/disable the convergence metrics or the solver (momentumsolver::convergence)</BriefDescription>
            </Void>
            <Double Name="eps" Label="Convergence criteria" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(momentumsolver::eps)</BriefDescription>
              <DefaultValue>1.0000000000000001e-05</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ppesolver" Label="ppesolver" BaseType="" Version="0" Unique="false">
      <ItemDefinitions>
        <Group Name="PressurePoissonSolver" Label="Pressure Poisson Solver" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="ppetype" Label="Type" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(ppesolver::type)</BriefDescription>
              <ChildrenDefinitions>
                <String Name="preconditioner" Label="Preconditioner" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>General</Cat>
                  </Categories>
                  <BriefDescription>(ppesolver::amgpc)</BriefDescription>
                  <ChildrenDefinitions>
                    <Int Name="agg_num_levels" Label="Number of Levels of Aggressive Coarsening" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::agg_num_levels)</BriefDescription>
                      <DefaultValue>0</DefaultValue>
                    </Int>
                    <Int Name="agg_num_paths" Label="Number of Paths of Aggressive Coarsening" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::agg_num_paths)</BriefDescription>
                      <DefaultValue>1</DefaultValue>
                    </Int>
                    <Int Name="coarse_size" Label="Minimum size of coarsest problem in AMG preconditioner" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::coarse_size)</BriefDescription>
                      <DefaultValue>500</DefaultValue>
                    </Int>
                    <String Name="cycle" Label="AMG Cycle" Version="0" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::cycle)</BriefDescription>
                      <DiscreteInfo DefaultIndex="0">
                        <Value Enum="V">V</Value>
                        <Value Enum="W">W</Value>
                      </DiscreteInfo>
                    </String>
                    <String Name="hypre_coarsen_type" Label="Coarsening Algorithm" Version="0" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::hypre_coarsen_type)</BriefDescription>
                      <DiscreteInfo DefaultIndex="3">
                        <Value Enum="Cleary-Luby-Jones-Plassman">CLJP</Value>
                        <Value Enum="Classical Ruge-Steuben on each processor, no boundary treatment">RUGE STEUBEN</Value>
                        <Value Enum="Classical Ruge-Steuben on each processor, followed by a third pass">MODIFIED RUGE STEUBEN</Value>
                        <Value Enum="Falgout coarsening">FALGOUT</Value>
                        <Value Enum="PMIS-coarsening">PMIS</Value>
                        <Value Enum="HMIS-coarsening">HMIS</Value>
                      </DiscreteInfo>
                    </String>
                    <Void Name="hypre_nodal" Label="Enable Nodal Systems Coarsening" Version="0" AdvanceLevel="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppseolver::hypre_nodal)</BriefDescription>
                    </Void>
                    <String Name="hypre_smoother" Label="Smoother" Version="0" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::hypre_smoother)</BriefDescription>
                      <DiscreteInfo DefaultIndex="4">
                        <Value Enum="Jacobi">JACOBI</Value>
                        <Value Enum="Gauss-Seidel, Sequential">SEQ_SGS</Value>
                        <Value Enum="Hybrid Gauss-Seidel or SOR, Forward Solve">HYB_GS</Value>
                        <Value Enum="Hybrid Gauss-Seidel or SOR, Backward Solve">BACK_HYB_GS</Value>
                        <Value Enum="Hybrid Symmetric Gauss-Seidel or SSOR">HYB_SGS</Value>
                        <Value Enum="Gaussian elimination">GE</Value>
                      </DiscreteInfo>
                    </String>
                    <String Name="hypre_smoother_co" Label="Smoother Coarsest" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::hypre_smoother_co)</BriefDescription>
                      <DiscreteInfo DefaultIndex="5">
                        <Value Enum="Jacobi">JACOBI</Value>
                        <Value Enum="Gauss-Seidel, Sequential">SEQ_SGS</Value>
                        <Value Enum="Hybrid Gauss-Seidel or SOR, Forward Solve">HYB_GS</Value>
                        <Value Enum="Hybrid Gauss-Seidel or SOR, Backward Solve">BACK_HYB_GS</Value>
                        <Value Enum="Hybrid Symmetric Gauss-Seidel or SSOR">HYB_SGS</Value>
                        <Value Enum="Gaussian Elimination">GE</Value>
                      </DiscreteInfo>
                    </String>
                    <String Name="hypre_smoother_dn" Label="Smoother Down" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::hypre_smoother_dn)</BriefDescription>
                      <DiscreteInfo DefaultIndex="4">
                        <Value Enum="Jacobi">JACOBI</Value>
                        <Value Enum="Gauss-Seidel, Sequential">SEQ_SGS</Value>
                        <Value Enum="Hybrid Gauss-Seidel or SOR, Forward Solve">HYB_GS</Value>
                        <Value Enum="Hybrid Gauss-Seidel or SOR, Backward Solve">BACK_HYB_GS</Value>
                        <Value Enum="Hybrid Symmetric Gauss-Seidel or SSOR">HYB_SGS</Value>
                        <Value Enum="Gaussian elimination">GE</Value>
                      </DiscreteInfo>
                    </String>
                    <String Name="hypre_smoother_up" Label="Smoother Up" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::hypre_smoother_up)</BriefDescription>
                      <DiscreteInfo DefaultIndex="4">
                        <Value Enum="Jacobi">JACOBI</Value>
                        <Value Enum="Gauss-Seidel, Sequential">SEQ_SGS</Value>
                        <Value Enum="Hybrid Gauss-Seidel or SOR, Forward Solve">HYB_GS</Value>
                        <Value Enum="Hybrid Gauss-Seidel or SOR, Backward Solve">BACK_HYB_GS</Value>
                        <Value Enum="Hybrid Symmetric Gauss-Seidel or SSOR">HYB_SGS</Value>
                        <Value Enum="Gaussian Elimination">GE</Value>
                      </DiscreteInfo>
                    </String>
                    <String Name="interp_type" Label="Parallel Interpolation Operator" Version="0" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::interp_type)</BriefDescription>
                      <DiscreteInfo DefaultIndex="0">
                        <Value Enum="Classical">CLASSICAL</Value>
                        <Value Enum="Direct">DIRECT</Value>
                        <Value Enum="Multipass">MULTIPASS</Value>
                        <Value Enum="Multipass with Separation of Weights">MULTIPASS WTS</Value>
                        <Value Enum="Extended+i">EXT+I</Value>
                        <Value Enum="Extended+i (if no Common C Neighbor)">EXT+I-CC</Value>
                        <Value Enum="Standard">STANDARD</Value>
                        <Value Enum="Standard with Separation of Weights">STANDARD WTS</Value>
                        <Value Enum="FF">FF</Value>
                        <Value Enum="FF1">FF1</Value>
                      </DiscreteInfo>
                    </String>
                    <Int Name="levels" Label="Maximum Number of AMG Levels" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::levels)</BriefDescription>
                      <DefaultValue>20</DefaultValue>
                    </Int>
                    <Double Name="max_rowsum" Label="Parameter to Modify the Definition of Strength for Diagonally Dominant Portions of the Matrix" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::max_rowsum)</BriefDescription>
                      <DefaultValue>0.90000000000000002</DefaultValue>
                    </Double>
                    <Int Name="pmax_elements" Label="Maximum Element/Row for the Interpolation" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::pmax_elements)</BriefDescription>
                      <DefaultValue>0</DefaultValue>
                    </Int>
                    <Int Name="post_smooth" Label="Number of Postsmoothing Sweeps" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::post_smooth)</BriefDescription>
                      <DefaultValue>1</DefaultValue>
                    </Int>
                    <Int Name="pre_smooth" Label="Number of Presmoothing Sweeps" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::pre_smooth)</BriefDescription>
                      <DefaultValue>1</DefaultValue>
                    </Int>
                    <String Name="smoother" Label="Smoother" Version="0" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::smoother)</BriefDescription>
                      <DiscreteInfo DefaultIndex="0">
                        <Value Enum="Incomplete Cholesky">ICC</Value>
                        <Value Enum="Incomplete LU">ILU</Value>
                        <Value Enum="Successive Over-Relaxation">SSOR</Value>
                        <Value Enum="Chebychev Polynomial">CHEBYCHEV</Value>
                      </DiscreteInfo>
                    </String>
                    <String Name="solver" Label="AMG Krylov Solver" Version="0" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::solver)</BriefDescription>
                      <DiscreteInfo DefaultIndex="0">
                        <Value Enum="Conjugate Gradient">CG</Value>
                        <Value Enum="Stabilized Bi-Conjugate Gradient Squared">BCGS</Value>
                        <Value Enum="Flexible Generalized Minimum Residual">FGMRES</Value>
                      </DiscreteInfo>
                    </String>
                    <Double Name="strong_threshold" Label="Strength Threshold" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::strong_threshold)</BriefDescription>
                      <DefaultValue>0.84999999999999998</DefaultValue>
                    </Double>
                    <Double Name="trunc_factor" Label="Interpolation Truncation Factor" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                      <Categories>
                        <Cat>General</Cat>
                      </Categories>
                      <BriefDescription>(ppesolver::trunc_factor)</BriefDescription>
                      <DefaultValue>0</DefaultValue>
                    </Double>
                  </ChildrenDefinitions>
                  <DiscreteInfo DefaultIndex="0">
                    <Structure>
                      <Value Enum="Multilevel Preconditioner (ML)">ml</Value>
                      <Items>
                        <Item>smoother</Item>
                        <Item>cycle</Item>
                        <Item>solver</Item>
                        <Item>pre_smooth</Item>
                        <Item>post_smooth</Item>
                        <Item>coarse_size</Item>
                        <Item>levels</Item>
                      </Items>
                    </Structure>
                    <Structure>
                      <Value Enum="Hypre">hypre</Value>
                      <Items>
                        <Item>hypre_coarsen_type</Item>
                        <Item>hypre_smoother</Item>
                        <Item>hypre_smoother_dn</Item>
                        <Item>hypre_smoother_up</Item>
                        <Item>hypre_smoother_co</Item>
                        <Item>interp_type</Item>
                        <Item>hypre_nodal</Item>
                        <Item>trunc_factor</Item>
                        <Item>pmax_elements</Item>
                        <Item>agg_num_levels</Item>
                        <Item>agg_num_paths</Item>
                        <Item>strong_threshold</Item>
                        <Item>max_rowsum</Item>
                        <Item>cycle</Item>
                        <Item>solver</Item>
                        <Item>pre_smooth</Item>
                        <Item>levels</Item>
                      </Items>
                    </Structure>
                  </DiscreteInfo>
                </String>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Structure>
                  <Value Enum="AMG">AMG</Value>
                  <Items>
                    <Item>preconditioner</Item>
                  </Items>
                </Structure>
                <Value Enum="SSORCG">SSORCG</Value>
                <Value Enum="JPCG">JPCG</Value>
              </DiscreteInfo>
            </String>
            <Int Name="itmax" Label="Maximum number of iterations" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>The maximum number of iterations. In the case of AMG, this is the maximum number of V or W cycles (ppesolver::itmax).</BriefDescription>
              <DefaultValue>500</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">500</Max>
              </RangeInfo>
            </Int>
            <Int Name="itchk" Label="Convergence criteria checking frequency" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>The number of iterations to take before checking convergence criteria (ppesolver::itchk).</BriefDescription>
              <DefaultValue>2</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">25</Max>
              </RangeInfo>
            </Int>
            <Void Name="diagnostics" Label="Diagnostics" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Enable/disable the diagnostic information from the linear solver (ppesolver::diagnostics)</BriefDescription>
            </Void>
            <Void Name="convergence" Label="Convergence metrics" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Enable/disable the convergence metrics or the linear solver (ppesolver::convergence)</BriefDescription>
            </Void>
            <Double Name="eps" Label="Convergence criteria" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(ppesolver::eps)</BriefDescription>
              <DefaultValue>1.0000000000000001e-05</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Double>
            <Double Name="pivot" Label="Preconditioner zero pivot value" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(ppesolver::zeropivot)</BriefDescription>
              <DefaultValue>9.9999999999999998e-17</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">0.10000000000000001</Max>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="simulationtime" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="nsteps" Label="Maximum number of time steps" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>The maximum number of time steps to be taken during a single simulation (nsteps)</BriefDescription>
          <DefaultValue>10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Double Name="term" Label="Simulation termination time" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>Define the simulation termination time, in units consistent with the problem definition (term)</BriefDescription>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
        <Double Name="deltat" Label="Time step size" Version="0" NumberOfRequiredValues="1">
          <Categories>
            <Cat>General</Cat>
          </Categories>
          <BriefDescription>Define the time step size to be used. This value may be over-ridden by physics specific constraints on the time step (deltat)</BriefDescription>
          <DefaultValue>0.01</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="solution_method" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="SolutionMethod" Label="Solution Method" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="strategy" Label="Strategy" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>solution_method::strategy</BriefDescription>
              <ChildrenDefinitions>
                <String Name="error_norm" Label="Non-linear convergence norm" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>General</Cat>
                  </Categories>
                  <BriefDescription>(solution_method::error_norm).</BriefDescription>
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="Composite RMS">composite</Value>
                    <Value Enum="Max">max</Value>
                  </DiscreteInfo>
                </String>
                <Int Name="nvec" Label="NVec" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
                  <Categories>
                    <Cat>General</Cat>
                  </Categories>
                  <BriefDescription>Maximum number of vectors used for non-linear Krylov acceleration (solution_method::nvec).</BriefDescription>
                  <DefaultValue>0</DefaultValue>
                </Int>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Projection">projection</Value>
                <Structure>
                  <Value Enum="Picard">picard</Value>
                  <Items>
                    <Item>error_norm</Item>
                    <Item>nvec</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
            <Int Name="itmax" Label="Maximum number of iterations" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>The maximum number of non-linear iterations to be taken during each time step (solution_method::itmax)</BriefDescription>
              <DefaultValue>5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Int>
            <Double Name="eps" Label="Convergence criteria" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(solution_method::eps)</BriefDescription>
              <DefaultValue>0.0001</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Double>
            <Double Name="eps_dist" Label="Convergence criteria for normal-distance function" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(solution_method::eps_dist)</BriefDescription>
              <DefaultValue>1.0000000000000001e-05</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Double>
            <Double Name="eps_p0" Label="Convergence criteria for initial div-free projection and initial pressure computation" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(solution_method::eps_p0)</BriefDescription>
              <DefaultValue>1.0000000000000001e-05</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Double>
            <Void Name="subcycle" Label="Subcycle the pressure solves" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Ignore pressure up-dates if the pressure variable has already converged (solution_method::subcycle)</BriefDescription>
            </Void>
            <Void Name="timestep_control" Label="Activate time step control" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(solution_method::timestep_control)</BriefDescription>
            </Void>
            <Void Name="convergence" Label="Convergence" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Enable/disable writing information about the non-linear convergence history to the conv file when using the Picard solution method (solution_method::convergence)</BriefDescription>
            </Void>
            <Void Name="diagnostics" Label="Diagnostics" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Enable/disable printing diagnostic information to the screen about the non-linear convergence history to the conv file when using the Picard solution method (solution_method::diagnostics)</BriefDescription>
            </Void>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="time_integration" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="TimeIntegration" Label="Time Integration" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="type" Label="Method" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(time_integration::type)</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Fixed CFL">fixed_cfl</Value>
                <Value Enum="Fixed time step">fixed_dt</Value>
              </DiscreteInfo>
            </String>
            <Double Name="CFLinit" Label="Initial CFL number to use at startup" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(time_integration::CFLinit)</BriefDescription>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">100</Max>
              </RangeInfo>
            </Double>
            <Double Name="CFLmax" Label="Maximum CFL number to use at startup" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(time_integration::CFLmax)</BriefDescription>
              <DefaultValue>2</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="dtmax" Label="Maximum time step" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(time_integration::dtmax)</BriefDescription>
              <DefaultValue>1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="dtscale" Label="Factor used to increase the time step" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(time_integration::dtscale)</BriefDescription>
              <DefaultValue>1.0249999999999999</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
              </RangeInfo>
            </Double>
            <Double Name="thetaa" Label="Time weight for advective terms" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Value of 0 indicates explicit advection and value of 1 indicates implicit (time_integration::thetaa)</BriefDescription>
              <DefaultValue>0.5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Double>
            <Double Name="thetaK" Label="Time weight for viscous/diffusive terms" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Value of 0 indicates explicit advection and value of 1 indicates implicit (time_integration::thetak)</BriefDescription>
              <DefaultValue>0.5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Double>
            <Double Name="thetaf" Label="Time weight for source terms" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Value of 0 indicates explicit treatment and value of 1 indicates implicit (time_integration::thetaf)</BriefDescription>
              <DefaultValue>0.5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Double>
            <Void Name="trimlast" Label="Use exact termination time" Version="0" Optional="true" IsEnabledByDefault="false">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(time_integration::trimlast)</BriefDescription>
            </Void>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="transportsolver" Label="Transport Solver" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="TransportSolver" Label="Transport Solver" Version="0" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="type" Label="Type" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(transportsolver::type)</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Flexible GMRES">FGMRES</Value>
                <Value Enum="ILU-Preconditioned FGMRES">ILUFGMRES</Value>
                <Value Enum="GMRES">GMRES</Value>
                <Value Enum="ILU-Preconditioned GMRES">ILUGMRES</Value>
              </DiscreteInfo>
            </String>
            <Int Name="restart" Label="Number of restart vectors" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(transportsolver::restart)</BriefDescription>
              <DefaultValue>30</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">100</Max>
              </RangeInfo>
            </Int>
            <Int Name="itmax" Label="Maximum number of iterations" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>The maximum number of iterations (transportsolver::itmax)</BriefDescription>
              <DefaultValue>500</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">500</Max>
              </RangeInfo>
            </Int>
            <Int Name="itchk" Label="Convergence criteria checking frequency" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>The number of iterations to take before checking convergence criteria (transportsolver::itchk)</BriefDescription>
              <DefaultValue>2</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">25</Max>
              </RangeInfo>
            </Int>
            <Void Name="diagnostics" Label="Diagnostics" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Enable/disable the diagnostic information from the solver (transportsolver::diagnostics)</BriefDescription>
            </Void>
            <Void Name="convergence" Label="Convergence metrics" Version="0" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>Enable/disable the convergence metrics or the solver (transportsolver::convergence)</BriefDescription>
            </Void>
            <Double Name="eps" Label="Convergence criteria" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
              <Categories>
                <Cat>General</Cat>
              </Categories>
              <BriefDescription>(transportsolver::eps)</BriefDescription>
              <DefaultValue>1.0000000000000001e-05</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0</Min>
                <Max Inclusive="true">1</Max>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <!--**********  Attribute Instances ***********-->
  <Attributes />
  <!--********** Workflow Views ***********-->
  <Views>
    <View Type="Group" Title="SimBuilder" TopLevel="true">
      <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
      <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
      <AdvancedFontEffects />
      <Views>
        <View Title="Materials" />
        <View Title="Source Terms" />
        <View Title="Execution" />
        <View Title="Problem Definition" />
        <View Title="Solvers" />
        <View Title="Field Output" />
        <View Title="Statistics" />
        <View Title="Boundary Conditions" />
        <View Title="Functions" />
      </Views>
    </View>
    <View Type="Attribute" Title="Boundary Conditions" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Type="BoundaryCondition" />
      </AttributeTypes>
    </View>
    <View Type="Instanced" Title="Execution">
      <InstancedAttributes>
        <Att Name="Load Balancer" Type="LoadBalancer" />
        <Att Name="Execution Control" Type="ExecutionControl" />
        <Att Name="Output" Type="Output" />
        <Att Name="Status Information" Type="StatusInformation" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Field Output">
      <AttributeTypes>
        <Att Type="VarOutput" />
      </AttributeTypes>
    </View>
    <View Type="SimpleExpression" Title="Functions">
      <Att Type="PolyLinearFunction" />
    </View>
    <View Type="Attribute" Title="Materials" ModelEntityFilter="r">
      <AttributeTypes>
        <Att Type="Material" />
      </AttributeTypes>
    </View>
    <View Type="Instanced" Title="Plot Window Parameters">
      <InstancedAttributes>
        <Att Name="Parameters" Type="TempStatVarStatistics" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Problem Definition">
      <InstancedAttributes>
        <Att Name="Simulation Time" Type="simulationtime" />
        <Att Name="Solution Method" Type="solution_method" />
        <Att Name="Time Integration" Type="time_integration" />
        <Att Name="Turbulence Model" Type="BasicTurbulenceModel" />
        <Att Name="energy" Type="energy" />
        <Att Name="Hydrostatic Pressure" Type="hydrostat" />
        <Att Name="Initial Conditions" Type="InitialConditions" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Solvers">
      <InstancedAttributes>
        <Att Name="Pressure Poisson Solver" Type="ppesolver" />
        <Att Name="Momentum Solver" Type="momentumsolver" />
        <Att Name="Transport Solver" Type="transportsolver" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Source Terms" ModelEntityFilter="r">
      <AttributeTypes>
        <Att Type="BodyForce" />
      </AttributeTypes>
    </View>
    <View Type="Group" Title="Statistics" Style="Tiled">
      <Views>
        <View Title="Plot Window Parameters" />
        <View Title="Variables" />
      </Views>
    </View>
    <View Type="Attribute" Title="Variables">
      <AttributeTypes>
        <Att Type="TempStatVarOutput" />
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeSystem>
